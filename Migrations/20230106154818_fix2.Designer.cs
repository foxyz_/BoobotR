﻿// <auto-generated />
using System;
using BoobotR;
using BoobotR.DB;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

#nullable disable

namespace Boobot.Migrations
{
    [DbContext(typeof(DataDbContext))]
    [Migration("20230106154818_fix2")]
    partial class fix2
    {
        /// <inheritdoc />
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder.HasAnnotation("ProductVersion", "7.0.1");

            modelBuilder.Entity("Boobot.DB.CustomDbTitle", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("INTEGER");

                    b.Property<DateTime>("DayOfWeek")
                        .HasColumnType("TEXT");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnType("TEXT");

                    b.HasKey("Id");

                    b.ToTable("custom_titles");
                });

            modelBuilder.Entity("Boobot.DB.DbEpisode", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("INTEGER");

                    b.Property<string>("Group")
                        .IsRequired()
                        .HasColumnType("TEXT");

                    b.Property<string>("Guid")
                        .IsRequired()
                        .HasColumnType("TEXT");

                    b.Property<string>("Link")
                        .IsRequired()
                        .HasColumnType("TEXT");

                    b.Property<float>("Number")
                        .HasColumnType("REAL");

                    b.Property<DateTime>("PubDate")
                        .HasColumnType("TEXT");

                    b.Property<int>("TitleId")
                        .HasColumnType("INTEGER");

                    b.HasKey("Id");

                    b.HasIndex("TitleId");

                    b.HasIndex(new[] { "Guid" }, "episodes_Guid_uindex")
                        .IsUnique();

                    b.ToTable("episodes");
                });

            modelBuilder.Entity("Boobot.DB.DbTitle", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("INTEGER");

                    b.Property<float>("LastEpisode")
                        .HasColumnType("REAL");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnType("TEXT");

                    b.HasKey("Id");

                    b.ToTable("titlers");
                });

            modelBuilder.Entity("Boobot.DB.Preset", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("INTEGER");

                    b.Property<float>("CfgScale")
                        .HasColumnType("REAL");

                    b.Property<int>("Height")
                        .HasColumnType("INTEGER");

                    b.Property<string>("Model")
                        .IsRequired()
                        .HasColumnType("TEXT");

                    b.Property<string>("NegativePrompt")
                        .IsRequired()
                        .HasColumnType("TEXT");

                    b.Property<ulong>("OwnerId")
                        .HasColumnType("INTEGER");

                    b.Property<string>("Prompt")
                        .IsRequired()
                        .HasColumnType("TEXT");

                    b.Property<bool>("Public")
                        .HasColumnType("INTEGER");

                    b.Property<string>("Sampler")
                        .IsRequired()
                        .HasColumnType("TEXT");

                    b.Property<int>("Seed")
                        .HasColumnType("INTEGER");

                    b.Property<int>("Steps")
                        .HasColumnType("INTEGER");

                    b.Property<int>("Width")
                        .HasColumnType("INTEGER");

                    b.HasKey("Id");

                    b.HasIndex("OwnerId");

                    b.ToTable("presets");
                });

            modelBuilder.Entity("Boobot.DB.ServerData", b =>
                {
                    b.Property<ulong>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("INTEGER");

                    b.Property<ulong?>("AnnouncementChannelId")
                        .HasColumnType("INTEGER");

                    b.HasKey("Id");

                    b.ToTable("servers");
                });

            modelBuilder.Entity("Boobot.DB.UserData", b =>
                {
                    b.Property<ulong>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("INTEGER");

                    b.Property<int?>("CurrentPresetId")
                        .HasColumnType("INTEGER");

                    b.Property<int>("eybrows")
                        .HasColumnType("INTEGER");

                    b.Property<int>("karma")
                        .HasColumnType("INTEGER");

                    b.HasKey("Id");

                    b.ToTable("users");
                });

            modelBuilder.Entity("Boobot.DB.DbEpisode", b =>
                {
                    b.HasOne("Boobot.DB.DbTitle", "Title")
                        .WithMany("Episodes")
                        .HasForeignKey("TitleId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Title");
                });

            modelBuilder.Entity("Boobot.DB.Preset", b =>
                {
                    b.HasOne("Boobot.DB.UserData", "Owner")
                        .WithMany("Presets")
                        .HasForeignKey("OwnerId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Owner");
                });

            modelBuilder.Entity("Boobot.DB.DbTitle", b =>
                {
                    b.Navigation("Episodes");
                });

            modelBuilder.Entity("Boobot.DB.UserData", b =>
                {
                    b.Navigation("Presets");
                });
#pragma warning restore 612, 618
        }
    }
}
