﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Boobot.Migrations
{
    /// <inheritdoc />
    public partial class renaming : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_episodes_titlers_TitleId",
                table: "episodes");

            migrationBuilder.DropTable(
                name: "DbTitleServerData");

            migrationBuilder.DropTable(
                name: "titlers");

            migrationBuilder.CreateTable(
                name: "titles",
                columns: table => new
                {
                    Id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Name = table.Column<string>(type: "TEXT", nullable: false),
                    LastEpisode = table.Column<float>(type: "REAL", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_titles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ServerDataTitle",
                columns: table => new
                {
                    FollowingServersId = table.Column<ulong>(type: "INTEGER", nullable: false),
                    TitlesId = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ServerDataTitle", x => new { x.FollowingServersId, x.TitlesId });
                    table.ForeignKey(
                        name: "FK_ServerDataTitle_servers_FollowingServersId",
                        column: x => x.FollowingServersId,
                        principalTable: "servers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ServerDataTitle_titles_TitlesId",
                        column: x => x.TitlesId,
                        principalTable: "titles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ServerDataTitle_TitlesId",
                table: "ServerDataTitle",
                column: "TitlesId");

            migrationBuilder.AddForeignKey(
                name: "FK_episodes_titles_TitleId",
                table: "episodes",
                column: "TitleId",
                principalTable: "titles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_episodes_titles_TitleId",
                table: "episodes");

            migrationBuilder.DropTable(
                name: "ServerDataTitle");

            migrationBuilder.DropTable(
                name: "titles");

            migrationBuilder.CreateTable(
                name: "titlers",
                columns: table => new
                {
                    Id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    LastEpisode = table.Column<float>(type: "REAL", nullable: false),
                    Name = table.Column<string>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_titlers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "DbTitleServerData",
                columns: table => new
                {
                    FollowingServersId = table.Column<ulong>(type: "INTEGER", nullable: false),
                    TitlesId = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DbTitleServerData", x => new { x.FollowingServersId, x.TitlesId });
                    table.ForeignKey(
                        name: "FK_DbTitleServerData_servers_FollowingServersId",
                        column: x => x.FollowingServersId,
                        principalTable: "servers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DbTitleServerData_titlers_TitlesId",
                        column: x => x.TitlesId,
                        principalTable: "titlers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_DbTitleServerData_TitlesId",
                table: "DbTitleServerData",
                column: "TitlesId");

            migrationBuilder.AddForeignKey(
                name: "FK_episodes_titlers_TitleId",
                table: "episodes",
                column: "TitleId",
                principalTable: "titlers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
