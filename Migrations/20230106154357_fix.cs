﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Boobot.Migrations
{
    /// <inheritdoc />
    public partial class fix : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_presets_users_OwnerID",
                table: "presets");

            migrationBuilder.DropForeignKey(
                name: "FK_users_presets_CurrentPresetID",
                table: "users");

            migrationBuilder.DropIndex(
                name: "IX_users_CurrentPresetID",
                table: "users");

            migrationBuilder.RenameColumn(
                name: "CurrentPresetID",
                table: "users",
                newName: "CurrentPresetId");

            migrationBuilder.RenameColumn(
                name: "OwnerID",
                table: "presets",
                newName: "OwnerId");

            migrationBuilder.RenameIndex(
                name: "IX_presets_OwnerID",
                table: "presets",
                newName: "IX_presets_OwnerId");

            migrationBuilder.AddForeignKey(
                name: "FK_presets_users_OwnerId",
                table: "presets",
                column: "OwnerId",
                principalTable: "users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_presets_users_OwnerId",
                table: "presets");

            migrationBuilder.RenameColumn(
                name: "CurrentPresetId",
                table: "users",
                newName: "CurrentPresetID");

            migrationBuilder.RenameColumn(
                name: "OwnerId",
                table: "presets",
                newName: "OwnerID");

            migrationBuilder.RenameIndex(
                name: "IX_presets_OwnerId",
                table: "presets",
                newName: "IX_presets_OwnerID");

            migrationBuilder.CreateIndex(
                name: "IX_users_CurrentPresetID",
                table: "users",
                column: "CurrentPresetID",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_presets_users_OwnerID",
                table: "presets",
                column: "OwnerID",
                principalTable: "users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_users_presets_CurrentPresetID",
                table: "users",
                column: "CurrentPresetID",
                principalTable: "presets",
                principalColumn: "Id");
        }
    }
}
