﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Boobot.Migrations
{
    /// <inheritdoc />
    public partial class sctSchemaFix : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_titlers_servers_ServerDataId",
                table: "titlers");

            migrationBuilder.DropIndex(
                name: "IX_titlers_ServerDataId",
                table: "titlers");

            migrationBuilder.DropColumn(
                name: "ServerDataId",
                table: "titlers");

            migrationBuilder.CreateTable(
                name: "DbTitleServerData",
                columns: table => new
                {
                    FollowingServersId = table.Column<ulong>(type: "INTEGER", nullable: false),
                    TitlesId = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DbTitleServerData", x => new { x.FollowingServersId, x.TitlesId });
                    table.ForeignKey(
                        name: "FK_DbTitleServerData_servers_FollowingServersId",
                        column: x => x.FollowingServersId,
                        principalTable: "servers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DbTitleServerData_titlers_TitlesId",
                        column: x => x.TitlesId,
                        principalTable: "titlers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_DbTitleServerData_TitlesId",
                table: "DbTitleServerData",
                column: "TitlesId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DbTitleServerData");

            migrationBuilder.AddColumn<ulong>(
                name: "ServerDataId",
                table: "titlers",
                type: "INTEGER",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_titlers_ServerDataId",
                table: "titlers",
                column: "ServerDataId");

            migrationBuilder.AddForeignKey(
                name: "FK_titlers_servers_ServerDataId",
                table: "titlers",
                column: "ServerDataId",
                principalTable: "servers",
                principalColumn: "Id");
        }
    }
}
