﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Boobot.Migrations
{
    /// <inheritdoc />
    public partial class serverSpecificTitles : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<ulong>(
                name: "ServerDataId",
                table: "titlers",
                type: "INTEGER",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_titlers_ServerDataId",
                table: "titlers",
                column: "ServerDataId");

            migrationBuilder.AddForeignKey(
                name: "FK_titlers_servers_ServerDataId",
                table: "titlers",
                column: "ServerDataId",
                principalTable: "servers",
                principalColumn: "Id");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_titlers_servers_ServerDataId",
                table: "titlers");

            migrationBuilder.DropIndex(
                name: "IX_titlers_ServerDataId",
                table: "titlers");

            migrationBuilder.DropColumn(
                name: "ServerDataId",
                table: "titlers");
        }
    }
}
