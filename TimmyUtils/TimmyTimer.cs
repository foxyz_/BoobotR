﻿using System.Timers;

namespace BoobotR.TimmyUtils
{
    public class TimmyTimer : System.Timers.Timer
    {
        DateTime StartTime { get; set; }
        public TimeSpan TimeSpanUntilEnd => StartTime + TimeSpan.FromMilliseconds(Interval) - DateTime.Now;


        public new void Start(double? interval = null) {
            if (interval != null) Interval = (double) interval;
            StartTime = DateTime.Now;
            base.Start();
        }
        public void StartRandom(int from, int to) {
            StartTime = DateTime.Now;
            Interval = new Random().Next(from, to);
            base.Start();
        }
        public TimmyTimer(ElapsedEventHandler? elapsed = null, double? interval = null, bool autoReset = false, bool enabled = false) {
            Enabled = enabled;
            AutoReset = autoReset;
            if (interval != null) Interval = (double) interval;
            if (elapsed != null) Elapsed += elapsed;
        } 
    }
}