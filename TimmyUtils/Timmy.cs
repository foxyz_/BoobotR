﻿using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;
using Discord.WebSocket;

namespace BoobotR.TimmyUtils
{
    public static class Timmy {
        public static int DayOfWeek(DateTime time) {
            return $"{time:ddd}".ToLower() switch {
                "mon" => 0,
                "tue" => 1,
                "wed" => 2,
                "thu" => 3,
                "fri" => 4,
                "sat" => 5,
                "sun" => 6,
                _ => 0
            };
        }

        public static Exception ConsoleThrower(string message) {
            Console.WriteLine(message);
            return new Exception(message);
        }
        public static DayOfWeek WeekOfDay(int index) {
            return index switch {
                0 => System.DayOfWeek.Monday,
                1 => System.DayOfWeek.Tuesday, 
                2 => System.DayOfWeek.Wednesday, 
                3 => System.DayOfWeek.Thursday, 
                4 => System.DayOfWeek.Friday, 
                5 => System.DayOfWeek.Saturday, 
                6 => System.DayOfWeek.Sunday, 
                _ => throw new IndexOutOfRangeException()
            };
        }

        public enum TimeEmbedType {
            Time,
            Remaining
        }
        public static string EmbedTime(DateTime from, TimeEmbedType type) {
            string end = type switch {TimeEmbedType.Time => "t", TimeEmbedType.Remaining => "R", _ => "t"};
            return $"<t:{GetUnixTimeStamp(from)}:{end}>";
        }
        public static int GetUnixTimeStamp(this DateTime from) {
            return (int)from.Subtract(new DateTime(1970, 1, 1)).TotalSeconds;
        }
        public static string ToPrettyForm(this TimeSpan t)
        {
            var shortForm = "";
            if (t.Days > 0)
            {
                shortForm += $"{t.Days.ToString()} days ";
            }
            if (t.Hours > 0)
            {
                shortForm += $"{t.Hours.ToString()} hours ";
            }
            if (t.Minutes > 0)
            {
                shortForm += $"{t.Minutes.ToString()} minutes ";
            }
            if (t.Seconds > 0)
            {
                shortForm += $"{t.Seconds.ToString()} seconds";
            }
            return shortForm;
        }
        public static int LevenshteinDistance(string s, string t) {
            int n = s.Length;
            int m = t.Length;
            int[,] d = new int[n + 1, m + 1];
            if (n == 0) {
                return m;
            }
            if (m == 0) {
                return n;
            }
            for (int i = 0; i <= n; d[i, 0] = i++)
                ;
            for (int j = 0; j <= m; d[0, j] = j++)
                ;
            for (int i = 1; i <= n; i++) {
                for (int j = 1; j <= m; j++) {
                    int cost = (t[j - 1] == s[i - 1]) ? 0 : 1;
                    d[i, j] = Math.Min(
                        Math.Min(d[i - 1, j] + 1, d[i, j - 1] + 1),
                        d[i - 1, j - 1] + cost);
                }
            }
            return d[n, m];
        }
        /*public static int ByTimmy (this DayOfWeek shit) {
            return shit switch {
                DayOfWeek.Monday => 0,
                DayOfWeek.Tuesday => 1,
                DayOfWeek.Wednesday => 2,
                DayOfWeek.Thursday => 3,
                DayOfWeek.Friday => 4,
                DayOfWeek.Saturday => 5,
                DayOfWeek.Sunday => 6,
                _ => 0
            };
        }*/
        public static string RussianWeeks (int count) {
            return count switch {
                0 => "Понедельник",
                1 => "Вторник",
                2 => "Среда",
                3 => "Четверг",
                4 => "Пятница",
                5 => "Суббота",
                6 => "Воскресенье",
                _ => "Хуета"
            };
        }
        public static string RussianHourCount (int hour) {
            var lastDigit = hour % 10;
            return lastDigit switch {
                > 4 => "Часов",
                > 1 => "Часа",
                1 => "Час",
                0 => "Часов",
                _ => throw new IndexOutOfRangeException()
            };
        }
        
        public static bool Chance (double percent, bool loud = false) {
            double roll = new Random().NextDouble();
            bool result = roll < percent * 0.01f;
            if (loud) Console.WriteLine($"rolled {roll * 100:F} out of {percent}% || result: {result}");
            return result;
        }

        public static string ToTitleCase(this string something)
        {
            return something.Substring(0, 1).ToUpper() + something.Substring(1);
        }

        public static string RussianDayCount (int hour) {
            var lastDigit = hour % 10;
            return lastDigit switch {
                > 4 => "Дней",
                > 1 => "Дня",
                1 => "День",
                0 => "Дней",
                _ => throw new IndexOutOfRangeException()
            };
        }
        public static string ChooseClosestMatching(IEnumerable<string> list, string input) {
            List<(int likeliness, string posString)> lis = new();
            (int likeliness, string s) mostLikely = (0, "");
            foreach (var pos in list) {
                var likeliness = (LevenshteinDistance(pos.ToLower(), input.ToLower()) - pos.Length) * -1;
                if (likeliness > mostLikely.likeliness) mostLikely = (likeliness, pos);
                lis.Add((likeliness, pos));
            }
            var likelinessList = (from v in lis where v.likeliness > 0 select v.likeliness).ToList();
            if (!likelinessList.Any()) {
                Console.WriteLine("no matches");
                return null;
            }
            var lCount = likelinessList.Count(l => l == mostLikely.likeliness);
            if (lCount <= 1) return mostLikely.s;
            Console.WriteLine("Uncertain");
            return null;
        }
        static bool CheckIfCommand(SocketMessage msg, out List<string> comand)
        {
            var mentions = new List<SocketUser>(msg.MentionedUsers);
            string msglc = msg.Content.ToLower();
            comand = new List<string>(msglc.Split());
            if (!msg.Content.StartsWith('!') && (!mentions.Any() || mentions.First().Id != Features.Discord.Client.CurrentUser.Id)) return false;
            if (Regex.IsMatch(comand[0], @"<(?:@[!&]?|#)\d+>")) comand.RemoveAt(0);
            if (msg.Content.StartsWith("!")) comand[0] = comand[0].TrimStart('!');
            return true;
        }

        public static TimeSpan TimeSince(DateTime ass) {
            return DateTime.Now - ass;
        }

        public static string ReadString(NetworkStream ns) {
            byte[] buffa = new byte[1024];
            int count = ns.Read(buffa);
            string msg = Encoding.UTF8.GetString(buffa, 0, count);
            return msg;
        }
    }
}