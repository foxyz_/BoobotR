using System.Net.Http.Json;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using BoobotR.DB;
using BoobotR.DB.API_Models;
using Discord;
using Discord.WebSocket;
using Newtonsoft.Json;

namespace BoobotR.Features;

public static class StableDiffusion {
    const string Adress = "http://senkochan.moe:7861/";
    const string Text2ImagePath = "sdapi/v1/txt2img";
    const string Image2ImagePath = "sdapi/v1/img2img";
    const string ModelsPath = "sdapi/v1/sd-models";
    const string OptionsPath = "sdapi/v1/options";

    public enum GenertionType {
        WebuiImg2Img,
        WebuiTxt2Img,
    }

    public static async Task Generate(SocketSlashCommand cmd, GenertionType type) {
        ulong userId = cmd.User.Id;
        await using var ctx = new DataDbContext();
        var user = await GetUser(ctx, userId);
        // Get user preset from the database or use the default one
        var preset = ctx.presets.FirstOrDefault(p => p.Id == user.CurrentPresetId) ?? new Preset();
        // var preset = user.CurrentPreset ?? new Preset();
        var parameters = new DiscordParameters(GetOptions(cmd), preset);
        
        await cmd.RespondAsync(embed: SlashCommander.Embeddber($"Generating a picture for '{parameters.Prompt}'..."));
        
        var modelResponse = DiscordGet(cmd, OptionsPath);

        var response = await DiscordPost(cmd, parameters.ImageUrl switch {null => Text2ImagePath, _ => Image2ImagePath} , await parameters.ToJsonBody());
        
        // Check if the request has succeeded
        if (!response.IsSuccessStatusCode) {
            await cmd.ModifyOriginalResponseAsync(properties => {
                properties.Embed = SlashCommander.Embeddber($"{(int) response.StatusCode} - {response.ReasonPhrase}");
            });
            return;
        }
        
        var generationResponse = JsonConvert.DeserializeObject<SdWebUiModels.GenerationModel>(await response.Content.ReadAsStringAsync())!;
        var responseInfo = JsonConvert.DeserializeObject<SdWebUiModels.GenerationInfo>(generationResponse.info)!;
        

        await cmd.ModifyOriginalResponseAsync(GenerationMessage);
        
        async void GenerationMessage(MessageProperties properties) {
            properties.Embed = await GenerationEmbed();
            properties.Attachments = ResponseImagesToAttachments(generationResponse.images);
            
            List<FileAttachment> ResponseImagesToAttachments(IEnumerable<string> base64Images) {
                string imageBase64 = base64Images.First();
                byte[] imageBytes = Convert.FromBase64String(imageBase64);

                return new List<FileAttachment> {new(new MemoryStream(imageBytes), "image.png")};
            }

            async Task<Embed> GenerationEmbed() {
                var builder = new StringBuilder();
                if (modelResponse.IsCompleted && modelResponse.Result.IsSuccessStatusCode) builder.AppendLine($"checkpoint: {JsonConvert.DeserializeObject<SdWebUiModels.ConfigModel>(await modelResponse.Result.Content.ReadAsStringAsync())!.sd_model_checkpoint}");
                if (type == GenertionType.WebuiImg2Img) {
                    builder.AppendLine($"image Url: {parameters.ImageUrl}");
                    builder.AppendLine($"denoising strength: {responseInfo.denoising_strength}");
                }
                if (!string.IsNullOrEmpty(responseInfo.negative_prompt)) builder.AppendLine($"negative: {responseInfo.negative_prompt}");
                builder.AppendLine($"cfg scale: {responseInfo.cfg_scale}");
                builder.AppendLine($"steps: {responseInfo.steps}");
                builder.AppendLine($"seed: {responseInfo.seed}");
            
                return SlashCommander.Embeddber($"{(string.IsNullOrEmpty(responseInfo.prompt) ? "<Empty>" : responseInfo.prompt)}", builder.ToString());
            }
        }
    }

    class DiscordParameters {
        IEnumerable<SocketSlashCommandDataOption> Options {get;}
        List<string>? images;
        public string? ImageUrl { get; }
        public float? DenoisingStrength { get; }
        public string? Prompt { get; }
        public string? NegativePrompt { get; }
        public float? CfgScale { get; }
        public int? Steps { get; }
        public int? Seed { get; }

        public DiscordParameters(IReadOnlyCollection<SocketSlashCommandDataOption> options, Preset preset) {
            Options = options;
            ImageUrl = options.GetOptionValue("image-url", preset.ImageUrl);
            DenoisingStrength = options.GetOptionValue("denoising-strength", preset.DenoisingStrength);
            Prompt = options.GetOptionValue("prompt", preset.Prompt);
            NegativePrompt = options.GetOptionValue("negative-prompt", preset.NegativePrompt);
            CfgScale = options.GetOptionValue("cfg-scale", preset.CfgScale);
            Steps = options.GetOptionValue("steps", preset.Steps);
            Seed = options.GetOptionValue("seed", preset.Seed); 
            
            // ImageUrl = GetOption("image-url", preset.ImageUrl)!;
            // DenoisingStrength = GetOption("denoising-strength", preset.DenoisingStrength);
            // Prompt = GetOption("prompt", preset.Prompt);
            // NegativePrompt = GetOption("negative-prompt", preset.NegativePrompt);
            // CfgScale = GetOption("cfg-scale", preset.CfgScale);
            // Steps = GetOption("steps", preset.Steps);
            // Seed = GetOption("seed", preset.Seed);
        }
        
        public DiscordParameters(IReadOnlyCollection<SocketSlashCommandDataOption> options) {
            Options = options;
            Prompt = GetOption<string?>("prompt");
            ImageUrl = GetOption<string?>("denoising-strength");
            NegativePrompt = GetOption<string?>("negative-prompt");
            CfgScale = GetOption<float?>("cfg-scale");
            DenoisingStrength = GetOption<float?>("denoising-strength");
            Steps = GetOption<int?>("steps");
            Seed = GetOption<int?>("seed");
        }
        
        T? GetOption<T>(string name, T? replacement = default) {
            var option = Options.FirstOrDefault(option => option.Name == name);
            if (option == null) {
                return replacement;
            }
            try {
                return option.Type switch {
                    ApplicationCommandOptionType.String => (T) option.Value,
                    ApplicationCommandOptionType.Integer => HandleInt(),
                    ApplicationCommandOptionType.Boolean => (T) option.Value,
                    ApplicationCommandOptionType.Number => HandleFloat(),
                    _ => throw Funny()
                };
            }
            catch (Exception) {
                Console.WriteLine("It is sad day");
                return replacement;
            }

            T HandleInt() {
                int fixedInt = Convert.ToInt32(option.Value);
                return (T) (object) fixedInt;
            }

            T HandleFloat() {
                double unFormatted = (double) option.Value;
                float converted = Convert.ToSingle(unFormatted);
                return (T) (object) converted;
            }

            Exception Funny() {
                Console.WriteLine("Unhandled type in 'GetOption' method");
                return new Exception("Unhandled type in 'GetOption' method");
            }
        }
        public async Task<JsonContent> ToJsonBody() { ;
            if (ImageUrl != null) images = new List<string> {await GetImageAsBase64Url(ImageUrl)}; // TODO handle if the image is bad
            return JsonContent.Create(new {
                init_images = images,
                denoising_strength = DenoisingStrength?.ToString(".00"),
                prompt = Prompt,
                negative_prompt = NegativePrompt,
                cfg_scale = CfgScale?.ToString(".0"),
                steps = Steps,
                seed = Seed,
            }, options: new JsonSerializerOptions{DefaultIgnoreCondition = JsonIgnoreCondition.WhenWritingNull});
        }
    }

    public static T? GetOptionValue<T>(this IEnumerable<SocketSlashCommandDataOption> options, string option, T? replacement = default) => (T?) Convert.ChangeType(options.FirstOrDefault(x => x.Name == option)?.Value ?? replacement, typeof(T));
    public static async Task UpdatePreset(SocketSlashCommand cmd) {
        var options = cmd.Data.Options.First().Options;
        ulong userId = cmd.User.Id;
        await using var ctx = new DataDbContext();
        var user = await GetUser(ctx, userId);

        var parameters = new DiscordParameters(options);

        if (user.CurrentPresetId == null) {
            ctx.presets.Add(new Preset{Owner = user});
            await ctx.SaveChangesAsync();
        }
        var preset = ctx.presets.First(p => p.Id == user.CurrentPresetId);
        user.CurrentPresetId = preset.Id;
        preset.Prompt = parameters.Prompt ?? preset.Prompt;
        preset.ImageUrl = parameters.ImageUrl ?? preset.ImageUrl;
        preset.NegativePrompt = parameters.NegativePrompt ?? preset.NegativePrompt;
        preset.CfgScale = parameters.CfgScale ?? preset.CfgScale;
        preset.DenoisingStrength = parameters.DenoisingStrength ?? preset.DenoisingStrength;
        preset.Steps = parameters.Steps ?? preset.Steps;
        preset.Seed = parameters.Seed ?? preset.Seed;
        await ctx.SaveChangesAsync();
        
        // if (user.CurrentPreset == null) {
        //     var newPreset = new Preset();
        //     if (prompt != null) newPreset.Prompt = prompt;
        //     if (cfgScale != null) newPreset.CfgScale = cfgScale.Value;
        //     if (steps != null) newPreset.Steps = steps.Value;
        //     if (seed != null) newPreset.Seed = seed.Value;
        //     user.CurrentPreset = new Preset();
        //     await ctx.SaveChangesAsync();
        // }
        // else {
        //     var preset = user.CurrentPreset;
        //     if (prompt != null) preset.Prompt = prompt;
        //     if (cfgScale != null) preset.CfgScale = cfgScale.Value;
        //     if (steps != null) preset.Steps = steps.Value;
        //     if (seed != null) preset.Seed = seed.Value;
        //     await ctx.SaveChangesAsync();
        // }

        await cmd.RespondAsync(embed: SlashCommander.Embeddber("Modified the current preset."), ephemeral: true);
        
        void ModifyPreset(Preset preset, DiscordParameters parameters) {
            preset.Prompt = parameters.Prompt ?? preset.Prompt;
            preset.ImageUrl = parameters.ImageUrl ?? preset.Prompt;
            preset.NegativePrompt = parameters.NegativePrompt ?? preset.NegativePrompt;
            preset.CfgScale = parameters.CfgScale ?? preset.CfgScale;
            preset.DenoisingStrength = parameters.DenoisingStrength ?? preset.DenoisingStrength;
            preset.Steps = parameters.Steps ?? preset.Steps;
            preset.Seed = parameters.Seed ?? preset.Seed;
        }
    }

    static IReadOnlyCollection<SocketSlashCommandDataOption> GetOptions(SocketSlashCommand cmd) {
        var options = cmd.Data.Options.First().Options.First().Options;
        return options;
    }

    static async Task<string> GetImageAsBase64Url(string url) {
        byte[] bytes = await Program.RequestClient.GetByteArrayAsync(url);
        return Convert.ToBase64String(bytes);
        //return "image/jpeg;base64," + Convert.ToBase64String(bytes);
    }

    public static async Task DefaultPreset(SocketSlashCommand cmd) {
        ulong userId = cmd.User.Id;
        await using var ctx = new DataDbContext();
        var user = await GetUser(ctx, userId);

        if (user.CurrentPresetId == null) {
            await cmd.RespondAsync(embed: SlashCommander.Embeddber("You are already using the defafult preset."),
                ephemeral: true);
            return;
        }

        user.CurrentPresetId = null;
        await ctx.SaveChangesAsync();
        await cmd.RespondAsync(embed: SlashCommander.Embeddber("Switched to default preset."), ephemeral: true);
        // if (user.CurrentPreset == null) {
        //     await cmd.RespondAsync(embed: SlashCommander.Embeddber("You are already using the defafult preset."), ephemeral:true);
        //     return;
        // }
        // user.CurrentPreset = null;
        // await ctx.SaveChangesAsync();
        // await cmd.RespondAsync(embed: SlashCommander.Embeddber("Switched to default preset."), ephemeral:true);
    }

    public static async Task ShowCurrentlyUsedPreset(SocketSlashCommand cmd) {
        var options = cmd.Data.Options.First().Options;
        ulong userId = cmd.User.Id;
        await using var ctx = new DataDbContext();
        var user = await GetUser(ctx, userId);

        bool ephemeral = false;
        if (options.Any()) {
            ephemeral = (bool?) options.FirstOrDefault(option => option.Name == "ephemeral") ?? false;
        }

        var preset = ctx.presets.FirstOrDefault(p => p.Id == user.CurrentPresetId);
        // var preset = user.CurrentPreset;

        if (preset != null) { // TODO show if empty
            var builder = new StringBuilder();
            if (preset.ImageUrl != null) builder.AppendLine($"Image URL: {preset.ImageUrl}");
            builder.AppendLine($"Prompt: {preset.Prompt}");
            builder.AppendLine($"Negative prompt: {preset.NegativePrompt}");
            builder.AppendLine($"Denoising strength: {preset.DenoisingStrength}");
            builder.AppendLine($"CFG scale: {preset.CfgScale}");
            builder.AppendLine($"Steps: {preset.Steps}");
            builder.AppendLine($"Seed: {preset.Seed}");
            await cmd.RespondAsync(embed: SlashCommander.Embeddber("Your current preset:", builder.ToString()),
                ephemeral: ephemeral);
        }
        else {
            preset = new Preset();
            var builder = new StringBuilder();
            builder.AppendLine($"Prompt: {preset.Prompt}");
            builder.AppendLine($"Negative prompt: {preset.NegativePrompt}");
            if (preset.ImageUrl != null) builder.AppendLine($"Image URL: {preset.ImageUrl}");
            builder.AppendLine($"Denoising strength: {preset.DenoisingStrength}");
            builder.AppendLine($"CFG scale: {preset.CfgScale}");
            builder.AppendLine($"Steps: {preset.Steps}");
            builder.AppendLine($"Seed: {preset.Seed}");
            await cmd.RespondAsync(
                embed: SlashCommander.Embeddber("You're using the default preset:", builder.ToString()),
                ephemeral: ephemeral);
        }
    }

    static async Task<UserData> GetUser(DataDbContext ctx, ulong userId) {
        var user = ctx.users.FirstOrDefault(data => data.Id == userId);
        if (user == null) {
            user = new UserData(userId);
            await ctx.users.AddAsync(user);
        }

        return user;
    }

    static async Task<HttpResponseMessage> DiscordGet(SocketSlashCommand cmd, string path) {
        try {
            return await Program.RequestClient.GetAsync(Adress + path);
        }
        catch (HttpRequestException e) {
            await cmd.RespondAsync(embed: SlashCommander.Embeddber("Stable diffusion is currently down", e.Message));
            throw;
        }
    }
    static async Task<HttpResponseMessage> DiscordPost(SocketInteraction cmd, string path, HttpContent body) {
        try {
            return await Program.RequestClient.PostAsync(Adress + path, body);
        }
        catch (HttpRequestException e) {
            await cmd.RespondAsync(embed: SlashCommander.Embeddber("Stable diffusion is currently down", e.Message));
            throw;
        }
    }
    public static async Task HandleModelCommand(SocketSlashCommand cmd) {
        var response = await DiscordGet(cmd, ModelsPath);
        
        if (!response.IsSuccessStatusCode) {
            await cmd.RespondAsync(embed: SlashCommander.Embeddber("There was an error doing the request."));
            return;
        }

        var availableModels =
            JsonConvert.DeserializeObject<List<SdWebUiModels.ModelModel>>(await response.Content.ReadAsStringAsync());

        if (availableModels == null) {
            await cmd.RespondAsync(embed: SlashCommander.Embeddber("There was an error doing the request."));
            return;
        }

        response = await Program.RequestClient.GetAsync(Adress + OptionsPath);
        if (!response.IsSuccessStatusCode) {
            await cmd.RespondAsync(embed: SlashCommander.Embeddber("There was an error doing the request."));
            return;
        }

        string funnyString = await response.Content.ReadAsStringAsync();

        var config = JsonConvert.DeserializeObject<SdWebUiModels.ConfigModel>(funnyString);

        if (config == null) {
            await cmd.RespondAsync(embed: SlashCommander.Embeddber("There was an error doing the request."));
            return;
        }

        var components = new ComponentBuilder().WithSelectMenu(new SelectMenuBuilder {
            CustomId = "model-select-menu",
            Options = availableModels.Select(model => new SelectMenuOptionBuilder(model.model_name, model.title,
                model.hash, isDefault: config.sd_model_checkpoint == model.title)).ToList()
        }).Build();

        await cmd.RespondAsync("(Switching a model can take a while, wait for a message that confirms it)\nPlease select a model:", components: components, ephemeral: true);
    }

    public static async Task HandleModelSelectMenu(SocketMessageComponent arg) {
        await arg.DeferAsync();
        string? model = arg.Data.Values.First();
        var body = JsonContent.Create(new {
            sd_model_checkpoint = model
        });
        var response = await DiscordPost(arg, OptionsPath, body);
        if (!response.IsSuccessStatusCode) {
            await arg.RespondAsync(embed: SlashCommander.Embeddber("There was an error doing the request"),
                ephemeral: true);
            return;
        }

        await arg.FollowupAsync(embed: SlashCommander.Embeddber($"'{model}' was selected as the current model!"), ephemeral: true);
    }
}