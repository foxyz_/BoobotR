﻿using System.Net;
using System.Text;
using BoobotR.DB.API_Models;
using Newtonsoft.Json;

namespace BoobotR.Features; 

public static class Vk {
    const string OurAddress = "http://192.168.1.178:80/";
    static HttpListener webServer = null!;
    public static Task Start() {
        try {
            webServer = new HttpListener();
            webServer.Prefixes.Add(OurAddress);
            webServer.Start();
        }
        catch (Exception e) {
            Console.WriteLine($"Webserver wasn't started \"{e.Message}\" type vk to try again");
            return Task.CompletedTask;
        }
        _ = Task.Run(() => {
            while (true)
            {
                HttpListenerContext context = webServer.GetContext();
                HttpListenerRequest req = context.Request;

                if (req.HttpMethod != "POST") {
                    Console.WriteLine("NOT POST");
                    Steal(req);
                    continue;
                }
                if (req.RawUrl != "/") {
                    Console.WriteLine("NOT RIGHT PATH");
                    Steal(req);
                    continue;
                }
                if (req.UserAgent != "Mozilla/5.0 (compatible; VKCallback/2.0; +https://vk.com/dev/callback_api)") {
                    Console.WriteLine("WHO?");
                    Steal(req);
                    continue;
                }
                if (!Discord.Connected) { // TODO Instead stock up on messages until its ready
                    continue;
                }
                VkModels.VkMessage? message;
                try {       
                    string body = new StreamReader(req.InputStream, req.ContentEncoding).ReadToEnd();       
                    ConfirmMessage(context.Response); // TODO Move?   
                    message = JsonConvert.DeserializeObject<VkModels.VkMessage>(body);
                }
                catch (Exception e) {
                    Console.WriteLine(e.Message);
                    continue;
                }
                if (message != null) {  
                    _ = Discord.RelayVkMessage(message);
                }
            }
        });
        Console.WriteLine($"Started webserver on {OurAddress}");
        return Task.CompletedTask;
    }

    static void Steal(HttpListenerRequest req) {
        // const string filename = "stolen.json";
        // string path = BotFiles.DataPath + filename;
        // if (!File.Exists(path)) {Console.WriteLine($"{path} doesn't exist, creating..."); File.Create(path).Close();}
        // File.WriteAllTextAsync(path ,JsonConvert.SerializeObject(req., Formatting.Indented, new JsonSerializerSettings {
        //     Error = (_, _) => { },
        //     
        // }));
    }

    static void ConfirmMessage(HttpListenerResponse response) {
        response.StatusCode = 200;
        response.Headers.Set("Content-Type", "text/plain");

        const string responseBodyString = "ok";
        byte[] buffer = Encoding.UTF8.GetBytes(responseBodyString);
        response.ContentLength64 = buffer.Length;
                
        using var ros = response.OutputStream;
        ros.Write(buffer, 0, buffer.Length);
    }
} 