using System.Globalization;
using System.Text.RegularExpressions;
using System.Xml;
using System.Xml.Serialization;
using BoobotR.DB;
using BoobotR.DB.API_Models;
using BoobotR.TimmyUtils;
using Discord;
using Discord.WebSocket;
using HtmlAgilityPack;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace BoobotR.Features; 
public static class Weeb { 
    const StringComparison Lc = StringComparison.InvariantCultureIgnoreCase;
    static readonly List<string> ReportedStuff = new();
    static bool announcedConnectionLost;
    static TimmyTimer checkTimer = null!;

    public static async Task Start() {
        checkTimer = new TimmyTimer((_, _) => _ = CheckForNewEpisodes(), TimeSpan.FromMinutes(5).TotalMilliseconds, true, true);
        await CheckForNewEpisodes();
    }

    static async Task CheckForNewEpisodes() {
        if (!Discord.Connected) {
            Console.WriteLine("Connection to discord was lost...");
            announcedConnectionLost = true;
            return;
        }
        if (announcedConnectionLost) {
            announcedConnectionLost = false;
            Console.WriteLine("Connected to discord.");
        }
        
        var newEpisodes = new List<Episode>();
        await using var ctx = new DataDbContext();
        var rssEpisodes = GetSubsPleaseItems();
        foreach (var episode in rssEpisodes) { 
            try {
                var dbTitle = ctx.titles.FirstOrDefault(title => title.Name == episode.Title.Name);
                if (dbTitle == null) { await ctx.titles.AddAsync(new Title(episode.Title.Name)); await ctx.SaveChangesAsync();}
                dbTitle = ctx.titles.First(title => title.Name == episode.Title.Name);
                if (episode.Number <= dbTitle.LastEpisode) continue;
                episode.Title = dbTitle;
                newEpisodes.Add(episode);
            }
            catch (Exception e) {
                Console.WriteLine(e.Message);
                throw;
            }
        }
        foreach (var episode in newEpisodes) {
            try {
                var dbTitle = ctx.titles.First(title => title.Name == episode.Title.Name);
                if (episode.Number > dbTitle.LastEpisode) dbTitle.LastEpisode = episode.Number;
                dbTitle.Episodes.Add(episode);
            }
            catch (Exception e) {
                Console.WriteLine(e.Message);
                throw;
            }
        }
        await ctx.SaveChangesAsync();
        foreach (var episode in newEpisodes) {
            await Discord.AnnounceNewEpisode(ctx, episode);
        }
    }
    static List<Episode> GetSubsPleaseItems()
    {
        var serializer = new XmlSerializer(typeof(Rss));
        var doc = new XmlDocument();
        try { 
            doc.Load("https://subsplease.org/rss/?t&r=1080");
        }
        catch (Exception e) {
            throw Timmy.ConsoleThrower($"There were several difficulties doing stuff '{e.Message}'");
        }
        string xml = doc.InnerXml;
        using var reader = new StringReader(xml);
        var items = ((Rss) serializer.Deserialize(reader)!).Channel.Item;
        //var items = test.Channel.Item.Where(x => BotFiles.LoadSettings().PassList.Any(y => x.Title.Contains(y, Lc))).ToList();
        var groupRegex = new Regex(@"\[(\w*)\] ");
        var episodes = new List<Episode>();
        foreach (var item in items) {
            var groupMatch = groupRegex.Match(item.Title);
            if (groupMatch.Success) {
                switch (groupMatch.Groups[1].Value) {
                    case "SubsPlease" or "EraiRaws":
                        var subsPleaseTitleRegex = new Regex(@"\[\w*\]\s(.*?)\s-\s(\d+(?:\.\d+)?)");
                        var titleMatch = subsPleaseTitleRegex.Match(item.Title);
                        if (!titleMatch.Success || !titleMatch.Groups[1].Success || !titleMatch.Groups[2].Success) {
                            if (ReportedStuff.Contains(item.Title)) continue;
                            Console.WriteLine($"Something has escaped a match from '{item.Title}'"); // TODO handle batch releases and OVAs
                            ReportedStuff.Add(item.Title);
                            continue;
                        }
                        episodes.Add(new Episode {
                            Title = new Title(titleMatch.Groups[1].Value),
                            Number = float.Parse(titleMatch.Groups[2].Value),
                            Group = groupMatch.Groups[1].Value,
                            PubDate = DateTime.Parse(item.PubDate, CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal),
                            Guid = item.Guid.Text,
                            Link = item.Link
                        });
                        break;
                    default: Console.WriteLine($"Unhandled group '{groupMatch.Groups[1].Value}' in {item.Title}"); break;
                }
            }
            else {
                Console.WriteLine($"Error parsing '{item.Title}' group");
            }
        }
        return episodes;
    }

    public static async Task<string> GetCoverPic(string name) {
        var doc = new HtmlDocument();
        doc.LoadHtml(await (await Program.RequestClient.GetAsync("https://subsplease.org/shows/")).Content.ReadAsStringAsync());
        string? href = doc.DocumentNode.SelectSingleNode($"//a[@title='{name}']").GetAttributeValue("href", null);
        doc.LoadHtml(await (await Program.RequestClient.GetAsync($"https://subsplease.org{href}")).Content.ReadAsStringAsync());
        return "https://subsplease.org" + doc.DocumentNode.SelectSingleNode("//img").GetAttributeValue("src", null);
    }

    static async Task<Embed> Screen(ulong serverId, Title title, bool isTracked) {
        var lastEpisode = title.Episodes.OrderBy(e => e.PubDate).Last();
        string? guildName = Discord.Client.GetGuild(serverId).Name;
        string imgUrl = await GetCoverPic(title.Name);
        return new EmbedBuilder {
            ImageUrl = imgUrl,
            Title = title.Name,
            Description = $"{title.LastEpisode} episodes ({title.Episodes.Count} accounted for)\n" +
                          $"Last episode Released at {Timmy.EmbedTime(lastEpisode.PubDate, Timmy.TimeEmbedType.Time)} {Timmy.EmbedTime(lastEpisode.PubDate, Timmy.TimeEmbedType.Remaining)}\n" +
                          $"{(isTracked ? "Is" : "Is not")} tracked on {guildName}",
            Color = new Color(107, 50, 168)
        }.Build();
    }

    static SelectMenuBuilder AnimeSelector(IEnumerable<Title> seasonalTitles, ServerData server, string currentlySelecteed) {
        var animeSelector = new SelectMenuBuilder{CustomId = "anime-selector" };
        foreach (var title in seasonalTitles) {
            string isTracked = server.Titles.Contains(title) ? "🟢" : "🔴";
            animeSelector.AddOption(title.Name, title.Name, title.LastEpisode + " episodes", isDefault: title.Name == currentlySelecteed, emote: new Emoji(isTracked));
            if (animeSelector.Options.Count >= 25) break;
        }
        return animeSelector;
    }

    static SelectMenuBuilder PageSelector(DataDbContext ctx, int currentPage = 1) {
        int seasonalTitles = GetRelevantTitles(ctx).Count; 
        int pageCount = (int)MathF.Ceiling(seasonalTitles / 25f);
        var selectMenu = new SelectMenuBuilder{CustomId = "anime-page-selector"};
        for (int i = 1; i <= pageCount; i++) {
            string iString = i.ToString();
            selectMenu.AddOption(new SelectMenuOptionBuilder(iString, iString, isDefault: currentPage == i));
        }
        return selectMenu;
    }
    public static async Task HandleAnimeSelectMenu(SocketMessageComponent arg) {
        await arg.DeferAsync();
        
        // Get page from embed
        var sm = (arg.Message.Components.ElementAt(1).Components.First() as SelectMenuComponent)!;
        int page = int.Parse(sm.Options.First(o => o.IsDefault != null && o.IsDefault.Value).Value);
        
        var ctx = new DataDbContext();
        var seasonalTitles = GetRelevantTitles(ctx, page);
        var server = ctx.servers.Where(s => s.Id == arg.GuildId).Include(s => s.Titles).First();
        var title = seasonalTitles.First(t => t.Name == arg.Data.Values.First());
        
        bool isTracked = title.FollowingServers.Contains(server);
        var fittingButton = isTracked ? new ButtonBuilder("Untrack", "anime-selector-untrack") : new ButtonBuilder("Track", "anime-selector-track"); 
        var componenets = new ComponentBuilder().WithSelectMenu(AnimeSelector(seasonalTitles, server, title.Name)).WithSelectMenu(PageSelector(ctx, page)).AddRow(new ActionRowBuilder().WithButton(fittingButton));
        var screen = await Screen(arg.GuildId!.Value, title, isTracked);
        await arg.ModifyOriginalResponseAsync(p => {
            p.Embed = screen;
            p.Components = componenets.Build();
        });
    }
    public static async Task HandlePageSelectMenu(SocketMessageComponent arg) {
        await arg.DeferAsync();
        
        // Get page from request
        int page = int.Parse(arg.Data.Values.First());
        
        var ctx = new DataDbContext();
        var seasonalTitles = GetRelevantTitles(ctx, page);
        var server = ctx.servers.Where(s => s.Id == arg.GuildId).Include(s => s.Titles).First();
        var title = seasonalTitles.First();        
        
        bool isTracked = title.FollowingServers.Contains(server);
        var fittingButton = isTracked ? new ButtonBuilder("Untrack", "anime-selector-untrack") : new ButtonBuilder("Track", "anime-selector-track"); 
        var componenets = new ComponentBuilder().WithSelectMenu(AnimeSelector(seasonalTitles, server, title.Name)).WithSelectMenu(PageSelector(ctx, page)).AddRow(new ActionRowBuilder().WithButton(fittingButton));
        var screen = await Screen(arg.GuildId!.Value, title, isTracked);
        await arg.ModifyOriginalResponseAsync(p => {
            p.Embed = screen;
            p.Components = componenets.Build();
        });
    }

    public static async Task Menu(SocketSlashCommand cmd) {
        
        var ctx = new DataDbContext();
        var seasonalTitles = GetRelevantTitles(ctx);
        var server = ctx.servers.Where(s => s.Id == cmd.GuildId).Include(s => s.Titles).First();
        var title = seasonalTitles.First();
        
        bool isTracked = title.FollowingServers.Contains(server);
        var fittingButton = isTracked ? new ButtonBuilder("Untrack", "anime-selector-untrack") : new ButtonBuilder("Track", "anime-selector-track"); 
        var componenets = new ComponentBuilder().WithSelectMenu(AnimeSelector(seasonalTitles, server, title.Name)).WithSelectMenu(PageSelector(ctx)).AddRow(new ActionRowBuilder().WithButton(fittingButton));
        await cmd.RespondAsync(embed: await Screen(cmd.GuildId!.Value, title, isTracked), components: componenets.Build(), ephemeral: true);
    }
    
    static List<Title> GetRelevantTitles(DataDbContext ctx, int? page = null) {
         var titles =  ctx.titles.Include(t => t.Episodes).Where(title => title.Episodes.Any()).ToList();
         return titles.Where(t => DateTime.Now - t.Episodes.Last().PubDate < TimeSpan.FromDays(8)).Skip(page != null ? (page.Value - 1) * 25 : 0).ToList();
    }

    public static async Task TrackTitle(SocketMessageComponent arg) {
        await arg.DeferAsync();
        
        // Get page for page selector
        var sm = (arg.Message.Components.ElementAt(1).Components.First() as SelectMenuComponent)!;
        int page = int.Parse(sm.Options.First(o => o.IsDefault != null && o.IsDefault.Value).Value);
        
        // Get title name for dbTitle
        string name = arg.Message.Embeds.First().Title;

        // Get current seasonal titles for screen, title for screen and selector
        var ctx = new DataDbContext();
        var seasonalTitles = GetRelevantTitles(ctx, page);
        var server = ctx.servers.Where(s => s.Id == arg.GuildId).Include(s => s.Titles).First();
        var title = ctx.titles.Where(t => t.Name == name).Include(t => t.FollowingServers).First(); //Include following servers only for this title
        
        // Tracking
        string? serverName = Discord.Client.Guilds.First(g => g.Id == server.Id).Name;
        if (title.FollowingServers.Contains(server)) {
            Console.WriteLine($"{serverName} is already following {title.Name}!");
            await arg.FollowupAsync($"{serverName} is already following {title.Name}!");
            return;
        }
        title.FollowingServers.Add(server);
        await GetMissingEpisodes(ctx, title.Name);
        await ctx.SaveChangesAsync();
        
        // Update UI | Has to know DB was already updated
        bool isTracked = title.FollowingServers.Contains(server);
        var fittingButton = isTracked ? new ButtonBuilder("Untrack", "anime-selector-untrack") : new ButtonBuilder("Track", "anime-selector-track"); 
        var componenets = new ComponentBuilder().WithSelectMenu(AnimeSelector(seasonalTitles, server, title.Name)).WithSelectMenu(PageSelector(ctx, page)).AddRow(new ActionRowBuilder().WithButton(fittingButton));
        var screen = await Screen(arg.GuildId!.Value, title, isTracked);
        await Discord.AnnounceNewFollow(ctx, title, serverName, server);
        await arg.ModifyOriginalResponseAsync(p => { p.Embed = screen; p.Components = componenets.Build(); });
    }

    public static async Task UntrackTitle(SocketMessageComponent arg) {
        await arg.DeferAsync();
        
        // Get page for page selector
        var sm = (arg.Message.Components.ElementAt(1).Components.First() as SelectMenuComponent)!;
        int page = int.Parse(sm.Options.First(o => o.IsDefault != null && o.IsDefault.Value).Value);
        
        // Get title name for dbTitle
        string name = arg.Message.Embeds.First().Title;

        // Get current seasonal titles for screen, title for screen and selector
        var ctx = new DataDbContext();
        var seasonalTitles = GetRelevantTitles(ctx, page);
        var server = ctx.servers.Where(s => s.Id == arg.GuildId).Include(s => s.Titles).First();
        var title = ctx.titles.Where(t => t.Name == name).Include(t => t.FollowingServers).First(); //Include following servers only for this title

        // Untrack
        string? serverName = Discord.Client.Guilds.First(g => g.Id == server.Id).Name;
        if (!title.FollowingServers.Contains(server)) {
            Console.WriteLine($"{serverName} is not even following {title.Name}!");
            await arg.FollowupAsync($"{serverName} is not even following {title.Name}!");
            return;
        }
        title.FollowingServers.Remove(server);
        await ctx.SaveChangesAsync();
        
        // Update UI | Has to know DB was already updated
        bool isTracked = title.FollowingServers.Contains(server);
        var fittingButton = isTracked ? new ButtonBuilder("Untrack", "anime-selector-untrack") : new ButtonBuilder("Track", "anime-selector-track"); 
        var componenets = new ComponentBuilder().WithSelectMenu(AnimeSelector(seasonalTitles, server, title.Name)).WithSelectMenu(PageSelector(ctx, page)).AddRow(new ActionRowBuilder().WithButton(fittingButton));
        var screen = await Screen(arg.GuildId!.Value, title, isTracked);
        await arg.ModifyOriginalResponseAsync(p => { p.Embed = screen; p.Components = componenets.Build(); });
    }

    static async Task GetMissingEpisodes(DataDbContext ctx, string titleName) {
        var doc = new HtmlDocument();
        var dbTitle = ctx.titles.Include(title => title.Episodes).First(title => title.Name == titleName);
        string fullUrl = "https://subsplease.org/shows/" + new Regex("[^a-zA-Z0-9]+").Replace(titleName, "-");
        doc.LoadHtml(await (await Program.RequestClient.GetAsync(fullUrl)).Content.ReadAsStringAsync());
        string? sid = doc.DocumentNode.SelectSingleNode("//table[@id='show-release-table']").GetAttributeValue("sid", null);
        var stuff = JsonConvert.DeserializeObject<Episodes>(await (await Program.RequestClient.GetAsync($"https://subsplease.org/api/?f=show&tz=Europe/Moscow&sid={sid}")).Content.ReadAsStringAsync())!;
        foreach (var episode in stuff.episodes.Values.TakeLast(26)) {
            string? dlLink = episode.downloads.Last().torrent;
            float ep = float.Parse(new Regex(@"v\d+").Replace(episode.episode, ""));
            if (dbTitle.Episodes.Exists(e => Math.Abs(e.Number - ep) < 0.1f)) continue;
            dbTitle.Episodes.Add(
                new Episode {
                    Link = dlLink,
                    Group = "SubsPlease",
                    Number = ep,
                    PubDate = DateTime.Parse(episode.release_date, CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal),
                });
        }
        await ctx.SaveChangesAsync();
    }
}
