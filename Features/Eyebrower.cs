using System.Timers;
using BoobotR.DB;
using BoobotR.TimmyUtils;
using Discord;
using Discord.WebSocket;
using Microsoft.EntityFrameworkCore;

namespace BoobotR.Features; 

public static class Eyebrower {
    #region CringeRespomder
    
    static readonly TimmyTimer WaitingTimer = new((_, _) => {
        lastId = null;
        lastMsg = null;
        Console.WriteLine("WaitingFailed");
    });
    static readonly TimmyTimer RealismTimer = new((_, _) => {
        if (!lastMsg!.Content.Contains("🤨")) {
            Console.WriteLine("NoRaisedEyebrow-Waitng 30000");
            WaitingTimer.Start(30000);
            return;
        }
        _ = lastMsg.Channel.SendMessageAsync("🤨");
        ready = false;
        CooldownTimer.Start(60000);
        Console.WriteLine("Cooldown...");
    });
    static readonly TimmyTimer CooldownTimer = new((_, _) => {
        Console.WriteLine("Ready");
        ready = true;
    });
    static bool ready = true;
    static SocketMessage? lastMsg; 
    static ulong? lastId; 

    #endregion

    #region BasedCurrency

    static readonly List<User> Users = new();
    static TimmyTimer eligibilityPeriod = new(autoReset:true, interval:300000, elapsed: ElegibilityTick, enabled: true);

    static void ElegibilityTick(object? sender, ElapsedEventArgs args) {
        foreach (var user in Users) {
            user.ElegibilityTick();
        }
    }

    #endregion
    static async Task Received(SocketMessage msg) {
        if (msg.Author.Id == Discord.Client.CurrentUser.Id || !ready) return;
        await HandleCurrency(msg.Content, msg.Author.Id, msg.Channel);
        await HandleResponding();
        
        Task HandleResponding() {
            if (Timmy.Chance(20)) return Task.CompletedTask;
            lastMsg = msg;
            if (RealismTimer.Enabled) {
                RealismTimer.Stop();
                RealismTimer.StartRandom(1000, 4000);
            }
            else if (WaitingTimer.Enabled) {
                if (msg.Author.Id != lastId && msg.Content.Contains("🤨")) {
                    RealismTimer.StartRandom(1000, 8000);
                    WaitingTimer.Stop();
                }
                else {
                    WaitingTimer.Stop();
                    WaitingTimer.Start(10000);
                }
            }
            else {
                if (!msg.Content.Contains("🤨")) return Task.CompletedTask;
                lastId = msg.Author.Id;
                WaitingTimer.Start(60000);
            }

            return Task.CompletedTask;
        }
    }

    public static async Task SafeGo(SocketMessage msg) { // TODO remove this fuckin garbage
        await Received(msg);
    }


    static async Task HandleCurrency(string content, ulong authorId, IMessageChannel channel) {
        if (!content.Contains("🤨")) return;
        if (Users.Any(u => u.Id == authorId)) {
            await Users.First(c => c.Id == authorId).AddEybrow(Discord.Users[authorId], channel);
        } else {
            Users.Add(await new User(authorId).AddEybrow(Discord.Users[authorId], channel));
        }
    }
    public static async void HandleSelectMenu(SocketMessageComponent arg) {
        var pageSelectMenu = new SelectMenuBuilder {
            Placeholder = "Choose user",
            CustomId = "eybrow/s1"
        };
        foreach (var user in Discord.Users.Values) {
            pageSelectMenu.Options.Add(new SelectMenuOptionBuilder().WithLabel(user.DisplayName).WithValue($"{user.Id}").WithDefault(arg.Data.Values.First() == $"{user.Id}"));
        }
        await arg.UpdateAsync(p => {
            if (arg.Message.Components != null) {
                var b = ComponentBuilder.FromComponents(arg.Message.Components);
                b.ActionRows[0].Components[0] = pageSelectMenu.Build();
                b.ActionRows[1].Components[0] = new ButtonBuilder("Selectus!", "eybrow/c").Build(); 
                p.Components = b.Build();
            }
            else {
                p.Content = "fuck";
            }
        });
    }
    public static async void HandleButtons(SocketMessageComponent arg) {
        switch (arg.Data.CustomId) {
            case "eybrow/c": await arg.RespondAsync($"Im gonna fucking kill you: {Discord.Users[GetIdFromDefault() ?? 841398698785243146].DisplayName}"); break;
            case "eybrow/donation": {
                await using var ctx = new DataDbContext();
                var dbUser = ctx.users.FirstOrDefault(d => d.Id.Equals(arg.User.Id));
                if (dbUser == null) {
                    await arg.RespondAsync("Unknown error!", ephemeral:true);
                    return;
                }
                if (dbUser.eybrows > 0) {
                    dbUser.eybrows--;
                    dbUser.karma++;
                    await ctx.SaveChangesAsync();
                    await arg.Channel.SendMessageAsync($"{Discord.Users[arg.User.Id].DisplayName} has donated 1 🤨 to the fund!");
                    await arg.UpdateAsync(properties => {
                        properties.Content = $"You have {dbUser.eybrows} 🤨";
                    });
                }
                else {
                    await arg.RespondAsync($"Sadly you don't have enough 🤨, how about you become a little..... Richer?", ephemeral:true); 
                }
                break;
            }
            default: await arg.RespondAsync("Unknown error!", ephemeral:true); break;
        }

        ulong? GetIdFromDefault() {
            var menu = (SelectMenuComponent) ComponentBuilder.FromComponents(arg.Message.Components).ActionRows[0].Components[0];
            foreach (var option in menu.Options) {
                if (option.IsDefault == null || !option.IsDefault.Value) continue;
                return ulong.Parse(option.Value);
            }
            return null;
        }
    }
    
    public static async Task HandleShopCommand(SocketSlashCommand cmd) {
        await using var ctx = new DataDbContext();
        var dbUser = ctx.users.FirstOrDefault(d => d.Id.Equals(cmd.User.Id));
        if (dbUser == null) {
            await cmd.RespondAsync("You don't exist 😔", ephemeral:true);
            return;
        }
        var pageSelectMenu = new SelectMenuBuilder {
            Placeholder = "Choose user",
            CustomId = "eybrow/s1"
        };
        foreach (var user in Discord.Users.Values) {
            pageSelectMenu.Options.Add(new SelectMenuOptionBuilder().WithLabel(user.DisplayName).WithValue($"{user.Id}"));
        }
        var rows = new List<ActionRowBuilder> {
            // new ActionRowBuilder().WithSelectMenu(pageSelectMenu),
            // new ActionRowBuilder().WithButton("Selectus!", "eyebrow/c", disabled: true),
            new ActionRowBuilder().WithButton("Do a donation - 1🤨", "eybrow/donation"),
        };
        var comps = new ComponentBuilder().WithRows(rows).Build();
        await cmd.RespondAsync($"You have {dbUser.eybrows} 🤨", components: comps, ephemeral: true);
    }
    public static async Task HandleProfileCommand(SocketSlashCommand cmd) {
        var user = cmd.Data.Options.FirstOrDefault() == null ? Discord.Users[cmd.User.Id] : Discord.Users[((SocketUser)cmd.Data.Options.First().Value).Id];
        
        await using var ctx = new DataDbContext();
        var dbUser = await ctx.users.FirstOrDefaultAsync(d => d.Id == user.Id);
        if (dbUser == null) {
            await cmd.RespondAsync(embed: new EmbedBuilder().WithAuthor(user).WithDescription("Don't exist").Build());
        }
        else {
            var fields = new List<EmbedFieldBuilder> {
                new EmbedFieldBuilder().WithName("Eybrows:").WithValue($"{dbUser.eybrows} ({(Users.FirstOrDefault(u => u.Id == user.Id) == null ? "5" : Users.First().EligibleFor)})").WithIsInline(true),
                new EmbedFieldBuilder().WithName("Karma:").WithValue($"{dbUser.karma}").WithIsInline(true),
            };
            await cmd.RespondAsync(embed: new EmbedBuilder().WithAuthor(user).WithFields(fields).Build());
        }
    }
    public static async Task HandleBoardCommand(SocketSlashCommand cmd) {
        await using var ctx = new DataDbContext();
        var fields = new List<EmbedFieldBuilder>();
        foreach (var user in ctx.users) {
            fields.Add(new EmbedFieldBuilder().WithName((await cmd.Channel.GetUserAsync(user.Id)).Username).WithValue(user.eybrows).WithIsInline(true));
        }
        fields = fields.OrderBy(builder => int.Parse((string)builder.Value)).ToList();
        await cmd.RespondAsync(embed: new EmbedBuilder().WithTitle("Eybrows leaderoard:").WithFields(fields).Build());
    }

    class User {
        public ulong Id { get; }
        public int EligibleFor { get; private set; }

        public User(ulong id) {
            Id = id;
            EligibleFor = 5;
        }

        public void ElegibilityTick() {
            if (EligibleFor < 5) EligibleFor++;
        }
        public async Task<User> AddEybrow(IGuildUser user, IMessageChannel channel) {
            if (EligibleFor < 1) {
                await channel.SendMessageAsync($"{user.Mention} <:neo_cringe:843738998315679754>");
                return this;
            }
            await using var ctx = new DataDbContext();
            var result = await ctx.users.FirstOrDefaultAsync(d => d.Id.Equals(Id));
            if (result == null) {
                var entryResult = await ctx.users.AddAsync(new UserData(Id));
                entryResult.Entity.eybrows++;
            }
            else {
                result.eybrows++;
            }
            EligibleFor--;
            await ctx.SaveChangesAsync();
            return this;
        }
    }

    public static async Task React(Cacheable<IUserMessage, ulong> arg1, Cacheable<IMessageChannel, ulong> arg2,
        SocketReaction arg3) => await HandleCurrency(arg3.Emote.Name, arg3.UserId, arg3.Channel);
}