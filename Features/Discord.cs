using System.Text;
using BoobotR.DB;
using BoobotR.DB.API_Models;
using Discord;
using Discord.WebSocket;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Exception = System.Exception;

namespace BoobotR.Features;

public static class Discord {
    public static readonly DiscordSocketClient Client = new();
    const string ConfigFilePath = "cfg.json";

    public static bool Connected => Client.ConnectionState == ConnectionState.Connected && Client.LoginState == LoginState.LoggedIn;
    public static bool Ready;

    static Dictionary<ulong, SocketTextChannel> announcementChannels = null!;
    static Dictionary<ulong, SocketTextChannel> vkChannels = null!;
    public static Dictionary<ulong, IGuildUser> Users = null!;


    public static async Task Start() {
        var config = GetConfig();
        try {
            await Client.LoginAsync(TokenType.Bot, config.Real ? config.MainlineToken : config.DevToken);
        }
        catch (Exception e) {
            Console.WriteLine(e);
            throw;
        }
        await Client.StartAsync();
        Client.Ready += ClientOnReady;
        Client.GuildAvailable += SlashCommander.InitializeCommands;
        Client.SlashCommandExecuted += SlashCommander.HandleSlashCommands;
        Client.MessageReceived += MessageReceived;
        Client.MessageReceived += Eyebrower.SafeGo;
        Client.ReactionAdded += Eyebrower.React;
        Client.ButtonExecuted += ButtonHub;
        Client.SelectMenuExecuted += SelectMenuHub;
        //Client.MessageReceived += DiscordCommandsHandler.CommandReceived;
    }
    
    // TODO Implement development stuff
    static Config GetConfig() {
        if (!File.Exists(ConfigFilePath)) {
            var sw = File.CreateText(ConfigFilePath);
            sw.Write(JsonConvert.SerializeObject(new Config()));
            sw.Flush();
            sw.Close();
            SetConfig();
        }
        var config = JsonConvert.DeserializeObject<Config>(File.ReadAllText(ConfigFilePath))!;
        Console.WriteLine($"Bot is currently in {(config.Real ? "Mainline" : "Dev")} mode");
        if (config.Real) {
            if (config.MainlineToken != "") return config;
            Console.WriteLine("Mainline token isn't set, input a mainline token:");
            string? token = null;
            while (string.IsNullOrEmpty(token)) {
                token = Console.ReadLine();
            }
        }
        else {
            if (config.DevToken != "") return config;
            Console.WriteLine("Dev token isn't set, input a dev token:");
            string? token = null;
            while (string.IsNullOrEmpty(token)) {
                token = Console.ReadLine();
            }
        }
        return config;
    }

    static void SetConfig() {
        bool set = false;
        var config = new Config();
        while (!set) {
            Console.WriteLine("Choose a mode:\n 1 - Mainline\n 2 - Dev");
            string? input = Console.ReadLine();
            switch (input) {
                case "1": {
                    config.Real = true;
                    set = true;
                    break;
                }
                case "2": {
                    config.Real = false;
                    set = true;
                    break;
                }
            }
        }
        Console.WriteLine("Input a mainline token:");
        string? token = null;
        while (string.IsNullOrEmpty(token)) {
            token = Console.ReadLine();
        }
        config.MainlineToken = token;
        Console.WriteLine("Input a dev token:");
        token = null;
        while (string.IsNullOrEmpty(token)) {
            token = Console.ReadLine();
        }
        config.DevToken = token;
        File.WriteAllText(ConfigFilePath, JsonConvert.SerializeObject(config));
    }

    class Config {
        public bool Real { get; set; } = false;
        public string MainlineToken { get; set; } = "";
        public string DevToken { get; set; } = "";
    }

    static Task SelectMenuHub(SocketMessageComponent arg) {
        string route = arg.Data.CustomId.Split('/').First();
        switch (route) {
            case "eybrow": Eyebrower.HandleSelectMenu(arg); break;
            case "model-select-menu": _ = StableDiffusion.HandleModelSelectMenu(arg); break;
            case "anime-selector": _ = Weeb.HandleAnimeSelectMenu(arg); break;
            case "anime-page-selector": _ = Weeb.HandlePageSelectMenu(arg); break;
        }
        return Task.CompletedTask;
    }

    static async Task ButtonHub(SocketMessageComponent arg) {
        string route = arg.Data.CustomId.Split('/').First();
        switch (route) {
            case "eybrow": Eyebrower.HandleButtons(arg); break;
            case "anime-selector-track": await Weeb.TrackTitle(arg); break;
            case "anime-selector-untrack": await Weeb.UntrackTitle(arg); break;
        }
    }

    static Task MessageReceived(SocketMessage msg) {
        if (msg.Author.Id == Client.CurrentUser.Id) return Task.CompletedTask;
        return Task.CompletedTask;
    }

    static async Task ClientOnReady() {
        try { 
            await SyncWithDb();
        }
        catch (Exception e) {
            Console.WriteLine(e);
            throw;
        }
        foreach (var v in announcementChannels.Values) {
            Console.WriteLine(v.Name);
        }
        
        await Vk.Start();
        await Weeb.Start();
        Ready = true;
    }

    public static async Task SetAnnouncementChannel(SocketSlashCommand cmd) {
        announcementChannels = new Dictionary<ulong, SocketTextChannel>();
        await using var ctx = new DataDbContext();
        ulong guildId = cmd.GuildId!.Value;
        
        // Update runtime list
        if (announcementChannels.ContainsKey(guildId)) {
            announcementChannels[guildId] = (SocketTextChannel) cmd.Channel;
        }
        else {
            announcementChannels.Add(guildId, (SocketTextChannel) cmd.Channel);
        }
        
        // Update database for future restarts
        var dbEntry = ctx.servers.FirstOrDefault(data => data.Id == guildId);
        if (dbEntry != null) {
            dbEntry.AnnouncementChannelId = cmd.ChannelId;
        }
        else {
            ctx.servers.Add(new ServerData(guildId) {AnnouncementChannelId = cmd.ChannelId});
        }
        await ctx.SaveChangesAsync();
        
        await cmd.RespondAsync(embed: SlashCommander.Embeddber($"Channel '{cmd.Channel.Name}' succesfully set as the annoncement channel!"));
    }

    public static async Task SetVkChannel(SocketSlashCommand cmd) {
        vkChannels = new Dictionary<ulong, SocketTextChannel>();
        await using var ctx = new DataDbContext();
        ulong guildId = cmd.GuildId!.Value;
        
        // Update runtime list
        if (vkChannels.ContainsKey(guildId)) {
            vkChannels[guildId] = (SocketTextChannel) cmd.Channel;
        }
        else {
            vkChannels.Add(guildId, (SocketTextChannel) cmd.Channel);
        }
        
        // Update database for future restarts
        var dbEntry = ctx.servers.FirstOrDefault(data => data.Id == guildId);
        if (dbEntry != null) {
            dbEntry.VkChannelId = cmd.ChannelId;
        }
        else {
            ctx.servers.Add(new ServerData(guildId) {VkChannelId = cmd.ChannelId}); // TODO FUCKING ENSURE CREATED
        }
        await ctx.SaveChangesAsync();
        
        await cmd.RespondAsync(embed: SlashCommander.Embeddber($"Channel '{cmd.Channel.Name}' succesfully set as the vk channel!"));
    }
    static async Task SyncWithDb() {
        Console.WriteLine("Loading DB data...");
        announcementChannels = new Dictionary<ulong, SocketTextChannel>();
        vkChannels = new Dictionary<ulong, SocketTextChannel>();
        await using var ctx = new DataDbContext();
        foreach (var server in Client.Guilds) {
            var dbEntry = ctx.servers.FirstOrDefault(data => data.Id == server.Id); 
            if (dbEntry == null) {
                ctx.servers.Add(new ServerData(server.Id)); // TODO Make ensure created method
            }
            else {
                if (dbEntry.AnnouncementChannelId != null) {
                    var channel = (SocketTextChannel?) server.Channels.FirstOrDefault(channel => channel.Id == dbEntry.AnnouncementChannelId);
                    if (channel == null) {
                        Console.WriteLine("Announcement channel not found!");
                        continue;
                    }
                    announcementChannels.Add(server.Id, channel);
                }
                if (dbEntry.VkChannelId != null) {
                    var channel = (SocketTextChannel?) server.Channels.FirstOrDefault(channel => channel.Id == dbEntry.VkChannelId);
                    if (channel == null) {
                        Console.WriteLine("VK channel not found!");
                        continue;
                    }
                    vkChannels.Add(server.Id, channel);
                }
            }
        }
        await ctx.SaveChangesAsync();        
        Console.WriteLine("Finised loading the DB.");
    }
    public static async Task AnnounceNewFollow(DataDbContext ctx, Title title, string serverName, ServerData server) {
        title = ctx.titles.Include(t => t.Episodes).First(t => t.Id == title.Id);
        
        Console.WriteLine($"{serverName} is now following {title.Name}");
        
        var embed = new EmbedBuilder {
            Title = $"{serverName} is now following \"{title.Name}\"",
            Color = new Color(69, 136, 230),
            ThumbnailUrl = await Weeb.GetCoverPic(title.Name),
        };
        StringBuilder sb = new();
        foreach (var episode in title.Episodes.TakeLast(12)) {
            sb.AppendLine($"[__**Episode {episode.Number}**__]({"https://nyaasi.final-spark.eu" + episode.Link[15..]})");
        }
        embed.AddField("**Download torrent:**", sb.ToString());
        embed.WithFooter(DateTime.Now.ToString("ddd, dd MMM yyy HH:mm:ss"));
        await announcementChannels.First(pair => pair.Key == server.Id).Value.SendMessageAsync(embed: embed.Build());
    }
    public static async Task AnnounceNewEpisode(DataDbContext ctx, Episode episode) {
        Console.WriteLine($"Came out episode {episode.Number} of {episode.Title.Name}");
        foreach (var announcementChannel in announcementChannels.Values) {
            var dbServer = ctx.servers.Include(s => s.Titles).First(server => server.Id == announcementChannel.Guild.Id);
            if (!dbServer.Titles.Contains(episode.Title)) return;
            var embed = new EmbedBuilder {
                Title = $"Came out episode {episode.Number} of '{episode.Title.Name}'",
                Color = new Color(69, 136, 230),
                ThumbnailUrl = await Weeb.GetCoverPic(episode.Title.Name),
            };
            embed.AddField("**Torrent**", $"[__**Download!**__]({"https://nyaasi.final-spark.eu" + episode.Link[15..]})");
            embed.WithFooter(episode.PubDate.ToString("ddd, dd MMM yyy HH:mm:ss"));
            await announcementChannel.SendMessageAsync(embed: embed.Build());
        }
    }

    public static async Task RelayVkMessage(VkModels.VkMessage request) {
        var message = request.@object.message;
        var stringBuilder = new StringBuilder();
        stringBuilder.AppendLine(message.text);
        var finalAttachments = await ExtractAttachments(request.@object.message.attachments);

        if (Ready) {
            foreach (var channel in vkChannels.Values) {
                _ = finalAttachments.Any() ? channel.SendFilesAsync(finalAttachments, stringBuilder.ToString()) : channel.SendMessageAsync(stringBuilder.ToString());
            }
        }
        else {
            var channel = (SocketTextChannel) Client.GetChannel(1062047940299997327);
            _ = finalAttachments.Any() ? channel.SendFilesAsync(finalAttachments, stringBuilder.ToString()) : channel.SendMessageAsync(stringBuilder.ToString());
        }
        async Task<List<FileAttachment>> ExtractAttachments(List<VkModels.Attachment> attachments) {
            var discordAttachments = new List<FileAttachment>();
            foreach (var attachment in attachments) {
                switch (attachment.type) {
                    case "sticker": {
                        byte[] bytes = await Program.RequestClient.GetByteArrayAsync(attachment.sticker.images.Last().url); // TODO Move Program.RequestClient
                        discordAttachments.Add(new FileAttachment(new MemoryStream(bytes), attachment.sticker.sticker_id + ".png"));
                        break;
                    } 
                    case "photo": {
                        byte[] bytes = await Program.RequestClient.GetByteArrayAsync(attachment.photo.sizes.Last().url); // TODO Move Program.RequestClient
                        discordAttachments.Add(new FileAttachment(new MemoryStream(bytes), attachment.photo.id + ".png"));
                        break;
                    }
                    case "video": {
                        string baseAdress = "https://vk.com/video";
                        var url = new StringBuilder(baseAdress);
                        if (attachment.video.owner_id != null) {
                            url.Append(attachment.video.owner_id);
                            url.Append('_');
                        }
                        url.Append(attachment.video.id); // https://vk.com/video-210682646_456241288
                        // Stopwatch t = Stopwatch.StartNew();
                        // double t1;
                        // double t2;
                        // double t3;
                        // var bytes = VkDownloader.GetVideoBytes(url.ToString());
                        // t1 = t.Elapsed.TotalMilliseconds;
                        // t = Stopwatch.StartNew();
                        // var bytes2 = VkDownloader.GetVideoBytesFfmpegless(url.ToString());
                        // t2 = t.Elapsed.TotalMilliseconds;
                        // t = Stopwatch.StartNew();
                        var bytes3 = await VkDownloader.GetVideoFileNormally(url.ToString());
                        // t3 = t.Elapsed.TotalMilliseconds;
                        // t.Stop();
                        attachment.video.title = Path.ChangeExtension(attachment.video.title, null);
                        discordAttachments.Add(new FileAttachment(new MemoryStream(bytes3), attachment.video.title + ".mp4"));
                        break;
                    } 
                    case "doc": {
                        byte[] bytes = await Program.RequestClient.GetByteArrayAsync(attachment.doc.url); // TODO Move Program.RequestClient
                        attachment.doc.title = Path.ChangeExtension(attachment.doc.title, null);
                        discordAttachments.Add(new FileAttachment(new MemoryStream(bytes), attachment.doc.title + "." + attachment.doc.ext)); // TODO add only if required
                        // switch (attachment.doc.ext) {
                        //     case "gif": {
                        //         byte[] bytes = await StableDiffusion.Program.RequestClient.GetByteArrayAsync(attachment.doc.url); // TODO Move Program.RequestClient
                        //         discordAttachments.Add(new FileAttachment(new MemoryStream(bytes), attachment.doc.title + "." + attachment.doc.ext));
                        //         break;
                        //     }
                        //     default: {
                        //         Console.WriteLine("unhandled doc type");
                        //         break;
                        //     }
                        // }
                        break;
                    } 
                    case "wall": {
                        discordAttachments.AddRange(await ExtractAttachments(attachment.wall.attachments));
                        break;
                    }
                }
            }
            return discordAttachments;
        }
    }

}