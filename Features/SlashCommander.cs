using System.Text;
using System.Text.RegularExpressions;
using BoobotR.DB;
using BoobotR.Depricated;
using BoobotR.TimmyUtils;
using Discord;
using Discord.WebSocket;
using Microsoft.EntityFrameworkCore;

namespace BoobotR.Features;

public static class SlashCommander {
    // new SlashCommandBuilder().WithName("add-custom-title").WithDescription("Add missing titles to weekdays")
    //     .AddOption("title-name", ApplicationCommandOptionType.String, "Title name")
    //     .AddOption("pub-date", ApplicationCommandOptionType.String, "Last episode Pubdate (2022-08-04 18:48)").Build(),

    public static async Task InitializeCommands(SocketGuild guild) {
        await guild.BulkOverwriteApplicationCommandAsync(new ApplicationCommandProperties[] {
            new SlashCommandBuilder().WithName("week").WithDescription("Shows weekly anime episode schedule").Build(),
            new SlashCommandBuilder().WithName("sd").WithDescription("Stable diffusion related commands")
                .AddOption(new SlashCommandOptionBuilder().WithName("model").WithDescription("Select currently used model").WithType(ApplicationCommandOptionType.SubCommand))
                .AddOption(new SlashCommandOptionBuilder().WithName("generate").WithDescription("Generate picture with the currently chosen preset").WithType(ApplicationCommandOptionType.SubCommandGroup)
                    .AddOption(new SlashCommandOptionBuilder().WithName("from-text").WithDescription("Genereate an image from just the text input").WithType(ApplicationCommandOptionType.SubCommand)
                        .AddOption("prompt", ApplicationCommandOptionType.String, "The prompt for the picture")
                        .AddOption("negative-prompt", ApplicationCommandOptionType.String, "What the ai should avoid")
                        .AddOption("cfg-scale", ApplicationCommandOptionType.Number, "How much the AI should follow the prompt")
                        .AddOption("steps", ApplicationCommandOptionType.Integer, "Amount of the iterations for the picture")
                        .AddOption("seed", ApplicationCommandOptionType.Integer, "Seed for the picture used in generation"))
                    .AddOption(new SlashCommandOptionBuilder().WithName("from-image").WithDescription("Generate an image from image and a text input").WithType(ApplicationCommandOptionType.SubCommand)
                        .AddOption("image-url", ApplicationCommandOptionType.String, "The image used as a base for generation", true)
                        .AddOption("denoising-strength", ApplicationCommandOptionType.Number, "How strong is the base image")
                        .AddOption("prompt", ApplicationCommandOptionType.String, "The prompt for the picture")
                        .AddOption("negative-prompt", ApplicationCommandOptionType.String, "What the ai should avoid")
                        .AddOption("cfg-scale", ApplicationCommandOptionType.Number, "How much the AI should follow the prompt")
                        .AddOption("steps", ApplicationCommandOptionType.Integer, "Amount of the iterations for the picture")
                        .AddOption("seed", ApplicationCommandOptionType.Integer, "Seed for the picture used in generation"))
                ).AddOption(new SlashCommandOptionBuilder().WithName("update-preset").WithDescription("Update currrently used preset").WithType(ApplicationCommandOptionType.SubCommand)
                    .AddOption("prompt", ApplicationCommandOptionType.String, "The prompt for the picture")
                    .AddOption("image-url", ApplicationCommandOptionType.String, "The image used as a base for generation")
                    .AddOption("denoising-strength", ApplicationCommandOptionType.Number, "How strong is the base image")
                    .AddOption("negative-prompt", ApplicationCommandOptionType.String, "What the ai should avoid")
                    .AddOption("cfg-scale", ApplicationCommandOptionType.Number, "How much the AI should follow the prompt")
                    .AddOption("steps", ApplicationCommandOptionType.Integer, "Amount of the iterations for the picture")
                    .AddOption("seed", ApplicationCommandOptionType.String, "Seed for the picture used in generation")
                ).AddOption(new SlashCommandOptionBuilder().WithName("default-preset").WithDescription("Use the default preset").WithType(ApplicationCommandOptionType.SubCommand)
                ).AddOption(new SlashCommandOptionBuilder().WithName("show-preset").WithDescription("Show the currently used preset").WithType(ApplicationCommandOptionType.SubCommand)
                    .AddOption("ephemeral", ApplicationCommandOptionType.Boolean, "Will the others see your preset")
                ).Build(),
            new SlashCommandBuilder().WithName("set-announcement").WithDescription("Set this channel as announcement channel.").Build(),
            new SlashCommandBuilder().WithName("set-vk").WithDescription("Set this channel as vk channel.").Build(),
            new SlashCommandBuilder().WithName("profile").WithDescription("Show profile")
                .AddOption("user", ApplicationCommandOptionType.User, "Which user", false).Build(),
            new SlashCommandBuilder().WithName("leaderboard").WithDescription("Show leaderboard").Build(),
            new SlashCommandBuilder().WithName("shop").WithDescription("Show shop UI").Build(),
            new SlashCommandBuilder().WithName("gel").WithDescription("Booba").Build(),
            new SlashCommandBuilder().WithName("weeb").WithDescription("Weeb related commands")
                // .AddOption(new SlashCommandOptionBuilder().WithName("1").WithDescription("Test1").WithType(ApplicationCommandOptionType.SubCommand))
                // .AddOption(new SlashCommandOptionBuilder().WithName("2").WithDescription("Test2").WithType(ApplicationCommandOptionType.SubCommand))
                .AddOption(new SlashCommandOptionBuilder().WithName("menu").WithDescription("Show menu with all current Seasonal Anime").WithType(ApplicationCommandOptionType.SubCommand)).Build(),
            new SlashCommandBuilder().WithName("check").WithDescription("Checker stuff")
                .AddOption(new SlashCommandOptionBuilder().WithName("append").WithDescription("Add a title name to check for").WithType(ApplicationCommandOptionType.SubCommand)
                    .AddOption("title", ApplicationCommandOptionType.String, "Title name", true))
                .AddOption(new SlashCommandOptionBuilder().WithName("remove").WithDescription("Remove entries from the check").WithType(ApplicationCommandOptionType.SubCommand)
                    .AddOption("title", ApplicationCommandOptionType.String, "Title name", true))
                    .AddOption(new SlashCommandOptionBuilder().WithName("view").WithDescription("View the check entries").WithType(ApplicationCommandOptionType.SubCommand))
                .AddOption(new SlashCommandOptionBuilder().WithName("custom").WithDescription("add or remove custom entries in /week").WithType(ApplicationCommandOptionType.SubCommandGroup)
                    .AddOption(new SlashCommandOptionBuilder().WithName("add").WithDescription("add a custom entry").WithType(ApplicationCommandOptionType.SubCommand)
                        .AddOption("title-name", ApplicationCommandOptionType.String, "Title name", isRequired:true)
                        .AddOption("pub-date", ApplicationCommandOptionType.String, "Last episode Pubdate (2022-08-04 18:48)", isRequired:true))
                    .AddOption(new SlashCommandOptionBuilder().WithName("remove").WithDescription("remove a custom entry").WithType(ApplicationCommandOptionType.SubCommand)
                        .AddOption("title-name", ApplicationCommandOptionType.String, "Title name", isRequired:true))
                ).Build()
        });
    }
    public static async Task HandleSlashCommands(SocketSlashCommand cmd) {
        switch (cmd.Data.Name) { 
            case "week": await HandleWeekCommand(); break;
            case "sd": await HandleSdCommands(); break;
            case "set-announcement": await Discord.SetAnnouncementChannel(cmd); break;
            case "set-vk": await Discord.SetVkChannel(cmd); break;
            case "gel": await Posts.HandleBooblers(cmd); break;
            case "weeb": await HandleWeebCommands(); break;
            // case "check": await HandleCheckCommands(); break;
            case "profile": await Eyebrower.HandleProfileCommand(cmd); break;
            case "shop": await Eyebrower.HandleShopCommand(cmd); break;
            case "leaderboard": await Eyebrower.HandleBoardCommand(cmd); break;
            default: Console.WriteLine("Unhandled Command"); break;
        }
        
        async Task HandleWeekCommand() {
            try {
                await using var ctx = new DataDbContext();
                var builder = new StringBuilder[7];
                foreach (var title in ctx.titles.Include(t => t.Episodes).Include(t => t.FollowingServers)) {
                    if (!title.Episodes.Any() || title.FollowingServers.All(server => server.Id != cmd.GuildId)) continue;
                    var lastEpisode = title.Episodes.Last();
                    
                    // Check if the last episode from the anime came out within 8 days
                    if (Timmy.TimeSince(lastEpisode.PubDate) > TimeSpan.FromDays(8)) continue;
                    
                    int dayIndex = Timmy.DayOfWeek(lastEpisode.PubDate);
                    builder[dayIndex] ??= new StringBuilder();
                    builder[dayIndex].AppendLine(
                        $"`{Regex.Replace(lastEpisode.Title.Name, @"^((?:\S+\s+){2}\S+).*", "${1}")}` <t:{lastEpisode.PubDate.GetUnixTimeStamp()}:t> <t:{lastEpisode.PubDate.GetUnixTimeStamp()}:R>");
                }

                // Adding custom titles
                foreach (var title in ctx.custom_titles) {
                    int dayIndex = Timmy.DayOfWeek(title.DayOfWeek);
                    builder[dayIndex] ??= new StringBuilder();
                    builder[dayIndex].AppendLine(
                        $"`{Regex.Replace(title.Name, @"^((?:\S+\s+){2}\S+).*", "${1}")}` <t:{title.DayOfWeek.GetUnixTimeStamp()}:t> `Custom`");
                }

                var megaBuilder = new StringBuilder();
                for (int i = 0; i < builder.Length; i++) {
                    if (builder[i] == null) continue;
                    //megaBuilder.AppendLine($"{new CultureInfo("ru-RU").DateTimeFormat.GetDayName(Timmy.WeekOfDay(i))}:");
                    megaBuilder.AppendLine($"{Timmy.WeekOfDay(i)}:");
                    megaBuilder.AppendLine(builder[i].ToString());
                }
                
                await cmd.RespondAsync(embed: Embeddber("Weekly anime schedule", megaBuilder.ToString()), ephemeral: true);
            }
            catch (Exception e) {
                Console.WriteLine(e);
                throw;
            }
        }

        async Task HandleWeebCommands() {
            switch (cmd.Data.Options.First().Name) {
                case "menu": await Weeb.Menu(cmd); break;
                case "2": break;
                case "3": break;
                default: Console.WriteLine($"Unhandled command: {cmd.Data.Options.First().Name}"); break;
            }
        }
        async Task HandleCheckCommands() {
            var settings = BotFiles.LoadSettings();
            switch (cmd.Data.Options.First().Name) {
                case "append": await HandleAppender(); break;
                case "remove": await HandleRemover(); break;
                case "view": await HandleViewer(); break;
                case "custom": await HandleCustoms(); break;
                default: Console.WriteLine($"Unhandled command: {cmd.Data.Options.First().Name}"); break;
            }
            async Task HandleAppender() {
                string? s = (cmd.Data.Options.First().Options.First().Value as string).ToLower();
                if (settings.PassList.Contains(s)) {
                    await cmd.RespondAsync(embed: Embeddber($"'{s}' already exists in the list"), ephemeral: true);
                }
                else {
                    settings.PassList.Add(s);
                    BotFiles.SaveSettings(settings);
                    await cmd.RespondAsync(embed: Embeddber($"'{s}' has been added to the list"));
                }
            } 
            async Task HandleRemover() {
                string? s = (cmd.Data.Options.First().Options.First().Value as string).ToLower();
                if (settings.PassList.Contains(s)) {
                    settings.PassList.Remove(s);
                    BotFiles.SaveSettings(settings);
                    await cmd.RespondAsync(embed: Embeddber($"'{s}' was removed from the list"));
                }
                else {
                    await cmd.RespondAsync(embed: Embeddber($"'{s}' doesn't exist"), ephemeral: true);
                }
            } 
            async Task HandleViewer() {
                var s = new StringBuilder();
                foreach (string pass in settings.PassList) s.AppendLine(pass);
                await cmd.RespondAsync(embed: Embeddber("Stuff whaterver:", $"{s}"), ephemeral: true);
            } 
            async Task HandleCustoms() {
                var options = cmd.Data.Options.First().Options.First().Options;
                switch (cmd.Data.Options.First().Options.First().Name) {
                    case "add": {
                        string name = (string) options.First(o => o.Name == "title-name");
                        var pubDate = DateTime.Parse((string) options.First(o => o.Name == "pub-date"));
                        
                        await using (var ctx = new DataDbContext()) {
                            await ctx.custom_titles.AddAsync(new CustomDbTitle { Name = name, DayOfWeek = pubDate});
                            await ctx.SaveChangesAsync();
                        }
                        await cmd.RespondAsync(embed: Embeddber($"Added '{name}' to weekdays"));
                        break;
                    }
                    case "remove": {
                        string name = options.First(o => o.Name == "title-name").Value as string;
                        await using (var ctx = new DataDbContext()) {
                            var foundEntry = ctx.custom_titles.FirstOrDefault(title => title.Name.ToLower() == name.ToLower());
                            if (foundEntry == null) {
                                await cmd.RespondAsync(embed: Embeddber($"{name} wasn't found in the database"), ephemeral: true);
                                return;
                            }
                            ctx.custom_titles.Remove(foundEntry);
                            await ctx.SaveChangesAsync();
                        }
                        await cmd.RespondAsync(embed: Embeddber($"Removed '{name}' from custom schedule"));
                        break;
                    }
                    default: Console.WriteLine($"Unhandled command: {cmd.Data.Options.First().Options.First().Name}"); break;
                }
            } 
        }
        async Task HandleSdCommands() {
            switch (cmd.Data.Options.First().Name) {
                case "generate": HandleGenerate(); break;
                case "model": await StableDiffusion.HandleModelCommand(cmd); break;
                case "update-preset": await StableDiffusion.UpdatePreset(cmd); break;
                case "default-preset": await StableDiffusion.DefaultPreset(cmd); break;
                case "show-preset": await StableDiffusion.ShowCurrentlyUsedPreset(cmd); break;
                default: Console.WriteLine("Unhandeled command."); break;

                void HandleGenerate() {
                    switch (cmd.Data.Options.First().Options.First().Name) {
                        case "from-image": _ = StableDiffusion.Generate(cmd, StableDiffusion.GenertionType.WebuiImg2Img); break;
                        case "from-text": _ = StableDiffusion.Generate(cmd, StableDiffusion.GenertionType.WebuiTxt2Img); break;
                        default: Console.WriteLine("Unhandeled command."); break;
                    }
                }
            }
        }
    }
    public static Embed Embeddber(string title, string? descripto = null) {
        return new EmbedBuilder {
            Title = title,
            Description = descripto,
            Color = new Color(107, 50, 168)
        }.Build();
    }
}
