﻿using System.Net;
using System.Net.Sockets;
using BoobotR.TimmyUtils;

namespace BoobotR.Features.Testing; 

public static class ChatServer {
    static readonly TcpListener Server = new(new IPEndPoint(IPAddress.Any, 6969));
    public static List<Channel> channels = channels = new List<Channel> {new("Chaah")};
    public static void StartServer() {
        Server.Start();
        Console.WriteLine("Started a socket server");
        _ = Task.Run(() => {
            while (true) {
                var client = Server.AcceptTcpClient();
                var ns = client.GetStream();
                var sw = new StreamWriter(ns) {AutoFlush = true};
                string username = Timmy.ReadString(client.GetStream());
                if (channels.FirstOrDefault(channel => channel.Users.Any(u => u.Username == username)) != null) {
                    sw.Write("nah my nigga");
                    sw.Close();
                    ns.Close();
                    client.Close();
                    continue;
                }
                Console.WriteLine($"{username} has connected");
                sw.Write("ok");
                var user = new User(client, ns, sw, username);
                channels.First().Users.Add(user);
                user.Listen();
                
            }
        });
    }

    public static async Task BroadcastMessageAsync(string message, string id) {
        var count = 0;
        foreach (var chan in channels) {
            if (!chan.Users.Any()) return;
            foreach (var user in chan.Users) {
                count++;
            }
        }
        Console.WriteLine($"Broadcasted to {count} users");
    }
}

public class Channel {
    string Name { get; set; }
    public List<User> Users { get; } = new();

    public Channel(string name) {
        Name = name;
    }
}

public class User {
    string Id { get;} = Guid.NewGuid().ToString();
    public string Username { get;}
    public TcpClient Client { get; }
    public NetworkStream Stream { get; }
    public StreamWriter Writer { get; }
    Task Listener { get; set; }

    public User(TcpClient client, NetworkStream stream, StreamWriter writer, string username) {
        Username = username;
        Client = client;
        Stream = stream;
        Writer = writer;
    }

    public void Listen() {
        Listener = Task.Run(() => {
            while (Client.Connected) {
                if(!Stream.DataAvailable) continue;
                string msg = Timmy.ReadString(Stream);
                Console.WriteLine($"{Username} said \"{msg}\"");
            } 
            Console.WriteLine("User Disconnected");
        });
    }
}