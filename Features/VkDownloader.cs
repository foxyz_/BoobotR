﻿using System.Diagnostics;

namespace BoobotR.Features
{
	public static class VkDownloader {
		static readonly SemaphoreSlim Semaphore = new(0);		
		static Process? porc;
		public static byte[] GetVideoBytes(string uri) {
			using var ytdl = CreateStream(new Uri(uri));
			using var ytdlOutput = ytdl.StandardOutput.BaseStream;
			using var memStream = new MemoryStream();
			ytdlOutput.CopyTo(memStream);
			return memStream.ToArray();
		}
		public static byte[] GetVideoBytesFfmpegless(string uri) {
			using var ytdl = CreateStream2(new Uri(uri));
			using var ytdlOutput = ytdl.StandardOutput.BaseStream;
			using var memStream = new MemoryStream();
			ytdlOutput.CopyTo(memStream);
			return memStream.ToArray();
		}
		public static MemoryStream GetMemoryStream(string uri) {
			using var ytdl = CreateStream(new Uri(uri));
			using var ytdlOutput = ytdl.StandardOutput.BaseStream;
			using var memStream = new MemoryStream();
			ytdlOutput.CopyTo(memStream);
			return memStream;
		}

		public static async Task<byte[]> GetVideoFileNormally(string toString) {
			const string tempVidPath = "temp/";
			const string tempVidName = "vid.mp4";
			string args = $" -o {tempVidPath}{tempVidName} --max-filesize 8m \"{toString}\""; 
			var procStartInfo = new ProcessStartInfo("yt-dlp.exe", args); 
			procStartInfo.UseShellExecute = false;
			procStartInfo.CreateNoWindow = true;
			if (porc != null) await Semaphore.WaitAsync();
			foreach (var file in Directory.GetFiles(tempVidPath)) {
				File.Delete(file);
			}
			porc = Process.Start(procStartInfo);
			await porc.WaitForExitAsync();
			var bytes = await File.ReadAllBytesAsync(tempVidPath + tempVidName);
			File.Delete(tempVidPath + tempVidName);
			porc = null;
			Semaphore.Release();
			return bytes;
		}

		static Process CreateStream(Uri youtubeLink)
		{
			string winYtdlCommandline = $"yt-dlp.exe --max-filesize 8M \"{youtubeLink}\" -o -";
			string winFfmpegCommandline = $"ffmpeg -hide_banner -i pipe: -strict -2 -ac 2 -f webm -c:v libvpx-vp9 -b:v 1M -acodec libopus pipe:1";
			var procStartInfo = new ProcessStartInfo("cmd", $"/c \"{winYtdlCommandline} | {winFfmpegCommandline}\"");
			procStartInfo.RedirectStandardOutput = true;
			procStartInfo.UseShellExecute = false;
			procStartInfo.CreateNoWindow = true;
			return Process.Start(procStartInfo);
		}

		static Process CreateStream2(Uri youtubeLink)
		{
			string winYtdlCommandline = $"yt-dlp.exe --max-filesize 8M \"{youtubeLink}\" -o -";
			var procStartInfo = new ProcessStartInfo("cmd", $"/c \"{winYtdlCommandline}");
			procStartInfo.RedirectStandardOutput = true;
			procStartInfo.UseShellExecute = false;
			procStartInfo.CreateNoWindow = true;
			return Process.Start(procStartInfo);
		}
	}
}