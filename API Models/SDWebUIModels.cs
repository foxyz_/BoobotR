﻿namespace BoobotR.API_Models; 

public class SdWebUiModels {
    public class GenerationInfo {
        public string prompt { get; set; }
        public List<string> all_prompts { get; set; }
        public string negative_prompt { get; set; }
        public List<string> all_negative_prompts { get; set; }
        public long seed { get; set; }
        public List<long> all_seeds { get; set; }
        public long subseed { get; set; }
        public List<long> all_subseeds { get; set; }
        public int subseed_strength { get; set; }
        public int width { get; set; }
        public int height { get; set; }
        public string sampler_name { get; set; }
        public double cfg_scale { get; set; }
        public int steps { get; set; }
        public int batch_size { get; set; }
        public bool restore_faces { get; set; }
        public object face_restoration_model { get; set; }
        public string sd_model_hash { get; set; }
        public int seed_resize_from_w { get; set; }
        public int seed_resize_from_h { get; set; }

        public float denoising_strength { get; set; }

        //public ExtraGenerationParams extra_generation_params { get; set; }
        public int index_of_first_image { get; set; }
        public List<string> infotexts { get; set; }
        public List<object> styles { get; set; }
        public string job_timestamp { get; set; }
        public int clip_skip { get; set; }
        public bool is_using_inpainting_conditioning { get; set; }
    }

    public class GenerationModel {
        public List<string> images { get; set; }
        public string info { get; set; }
    }

    public class ConfigModel {
        public bool samples_save { get; set; }
        public string samples_format { get; set; }
        public string samples_filename_pattern { get; set; }
        public bool save_images_add_number { get; set; }
        public bool grid_save { get; set; }
        public string grid_format { get; set; }
        public bool grid_extended_filename { get; set; }
        public bool grid_only_if_multiple { get; set; }
        public bool grid_prevent_empty_spots { get; set; }
        public double n_rows { get; set; }
        public bool enable_pnginfo { get; set; }
        public bool save_txt { get; set; }
        public bool save_images_before_face_restoration { get; set; }
        public bool save_images_before_highres_fix { get; set; }
        public bool save_images_before_color_correction { get; set; }
        public double jpeg_quality { get; set; }
        public bool export_for_4chan { get; set; }
        public bool use_original_name_batch { get; set; }
        public bool use_upscaler_name_as_suffix { get; set; }
        public bool save_selected_only { get; set; }
        public bool do_not_add_watermark { get; set; }
        public string temp_dir { get; set; }
        public bool clean_temp_dir_at_start { get; set; }
        public string outdir_samples { get; set; }
        public string outdir_txt2img_samples { get; set; }
        public string outdir_img2img_samples { get; set; }
        public string outdir_extras_samples { get; set; }
        public string outdir_grids { get; set; }
        public string outdir_txt2img_grids { get; set; }
        public string outdir_img2img_grids { get; set; }
        public string outdir_save { get; set; }
        public bool save_to_dirs { get; set; }
        public bool grid_save_to_dirs { get; set; }
        public bool use_save_to_dirs_for_ui { get; set; }
        public string directories_filename_pattern { get; set; }
        public double directories_max_prompt_words { get; set; }
        public double ESRGAN_tile { get; set; }
        public double ESRGAN_tile_overlap { get; set; }
        public List<string> realesrgan_enabled_models { get; set; }
        public object upscaler_for_img2img { get; set; }
        public int ldsr_steps { get; set; }
        public bool ldsr_cached { get; set; }
        public int SWIN_tile { get; set; }
        public int SWIN_tile_overlap { get; set; }
        public object face_restoration_model { get; set; }
        public double code_former_weight { get; set; }
        public bool face_restoration_unload { get; set; }
        public double memmon_poll_rate { get; set; }
        public bool samples_log_stdout { get; set; }
        public bool multiple_tqdm { get; set; }
        public bool unload_models_when_training { get; set; }
        public bool pin_memory { get; set; }
        public bool save_optimizer_state { get; set; }
        public string dataset_filename_word_regex { get; set; }
        public string dataset_filename_join_string { get; set; }
        public double training_image_repeats_per_epoch { get; set; }
        public double training_write_csv_every { get; set; }
        public bool training_xattention_optimizations { get; set; }
        public string sd_model_checkpoint { get; set; }
        public double sd_checkpoint_cache { get; set; }
        public double sd_vae_checkpoint_cache { get; set; }
        public string sd_vae { get; set; }
        public bool sd_vae_as_default { get; set; }
        public string sd_hypernetwork { get; set; }
        public double sd_hypernetwork_strength { get; set; }
        public double inpainting_mask_weight { get; set; }
        public double initial_noise_multiplier { get; set; }
        public bool img2img_color_correction { get; set; }
        public bool img2img_fix_steps { get; set; }
        public string img2img_background_color { get; set; }
        public bool enable_quantization { get; set; }
        public bool enable_emphasis { get; set; }
        public bool enable_batch_seeds { get; set; }
        public double comma_padding_backtrack { get; set; }
        public double CLIP_stop_at_last_layers { get; set; }
        public List<object> random_artist_categories { get; set; }
        public bool use_old_emphasis_implementation { get; set; }
        public bool use_old_karras_scheduler_sigmas { get; set; }
        public bool interrogate_keep_models_in_memory { get; set; }
        public bool interrogate_use_builtin_artists { get; set; }
        public bool interrogate_return_ranks { get; set; }
        public double interrogate_clip_num_beams { get; set; }
        public double interrogate_clip_min_length { get; set; }
        public double interrogate_clip_max_length { get; set; }
        public double interrogate_clip_dict_limit { get; set; }
        public double interrogate_deepbooru_score_threshold { get; set; }
        public bool deepbooru_sort_alpha { get; set; }
        public bool deepbooru_use_spaces { get; set; }
        public bool deepbooru_escape { get; set; }
        public string deepbooru_filter_tags { get; set; }
        public bool show_progressbar { get; set; }
        public double show_progress_every_n_steps { get; set; }
        public string show_progress_type { get; set; }
        public bool show_progress_grid { get; set; }
        public bool return_grid { get; set; }
        public bool do_not_show_images { get; set; }
        public bool add_model_hash_to_info { get; set; }
        public bool add_model_name_to_info { get; set; }
        public bool disable_weights_auto_swap { get; set; }
        public bool send_seed { get; set; }
        public bool send_size { get; set; }
        public string font { get; set; }
        public bool js_modal_lightbox { get; set; }
        public bool js_modal_lightbox_initially_zoomed { get; set; }
        public bool show_progress_in_title { get; set; }
        public bool samplers_in_dropdown { get; set; }
        public bool dimensions_and_batch_together { get; set; }
        public string quicksettings { get; set; }
        public string ui_reorder { get; set; }
        public string localization { get; set; }
        public List<object> hide_samplers { get; set; }
        public double eta_ddim { get; set; }
        public double eta_ancestral { get; set; }
        public string ddim_discretize { get; set; }
        public double s_churn { get; set; }
        public double s_tmin { get; set; }
        public double s_noise { get; set; }
        public double eta_noise_seed_delta { get; set; }
        public List<object> disabled_extensions { get; set; }
    }

    public class ModelModel {
        public string title { get; set; }
        public string model_name { get; set; }
        public string hash { get; set; }
        public string filename { get; set; }
        public string config { get; set; }
    }
}