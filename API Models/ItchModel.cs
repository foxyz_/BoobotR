using System.Xml.Serialization;

namespace BoobotR.API_Models; 

public class ItchModel {
// using System.Xml.Serialization;
// XmlSerializer serializer = new XmlSerializer(typeof(Rss));
// using (StringReader reader = new StringReader(xml))
// {
//    var test = (Rss)serializer.Deserialize(reader);
// }
    [XmlRoot(ElementName="item")]
    public class Item { 

        [XmlElement(ElementName="title")] 
        public string Title { get; set; } 

        [XmlElement(ElementName="link")] 
        public string Link { get; set; } 

        [XmlIgnore]
        public DateTime Date { get; set; }

        [XmlElement(ElementName="pubDate")] 
        public string DateString
        {
            get => Date.ToString("yyyy-MM-dd HH:mm:ss");
            set => Date = DateTime.Parse(value);
        }
        
        [XmlElement(ElementName="category")] 
        public string Category { get; set; } 

        [XmlElement(ElementName="description")] 
        public string Description { get; set; } 
    }

    [XmlRoot(ElementName="channel")]
    public class Channel { 

        [XmlElement(ElementName="title")] 
        public string Title { get; set; } 

        [XmlElement(ElementName="link")] 
        public string Link { get; set; } 

        [XmlElement(ElementName="description")] 
        public string Description { get; set; } 

        [XmlElement(ElementName="item")] 
        public List<Item> Items { get; set; } 
    }

    [XmlRoot(ElementName="rss")]
    public class Rss { 

        [XmlElement(ElementName="channel")] 
        public Channel Channel { get; set; } 

        [XmlAttribute(AttributeName="version")] 
        public double Version { get; set; } 

        [XmlText] 
        public string Text { get; set; } 
    }
}