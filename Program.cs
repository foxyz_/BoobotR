﻿using System.Text;
using BoobotR.DB;
using BoobotR.Features.Testing;
using Microsoft.EntityFrameworkCore;

namespace BoobotR.Features;
public static class Program {
    public static bool IsRunning { get; } = true;
    public static readonly HttpClient RequestClient = new();

    
    static async Task Main() {
        Console.OutputEncoding = Encoding.UTF8;
        ChatServer.StartServer();
        await Discord.Start();
        
        if (!Directory.Exists("Data")) Directory.CreateDirectory("Data");

        while (true) {
            string? input = Console.ReadLine();
            switch (input?.Trim().ToLower()) {
                case "status": Console.WriteLine(Discord.Client.Status); break; 
                case "login": Console.WriteLine(Discord.Client.LoginState); break;
                case "state": Console.WriteLine(Discord.Client.ConnectionState); break;
                case "vk":  await Vk.Start(); break;
                case "test": {
                    var ctx = new DataDbContext();
                    var sus = ctx.servers.Include(s => s.Titles).First(s => s.Id == 1001615158017261698).Titles;
                    break;
                };
                case "r": {
                    StringBuilder sb = new();
                    var user = ChatServer.channels.First().Users.First();
                    sb.AppendLine($"connected: {user.Client.Connected}");
                    sb.AppendLine($"stream: {user.Stream}");
                    sb.AppendLine($"sw: {user.Writer}");
                    Console.WriteLine(sb.ToString());
                    break;
                }
                default: Console.WriteLine(":raised_eyebrow:"); break;
            }
        }
    }
}