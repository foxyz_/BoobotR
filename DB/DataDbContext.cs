using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;

namespace BoobotR.DB; 

public class DataDbContext : DbContext{
        
    public DbSet<Title> titles { get; set; }
    public DbSet<Episode> episodes { get; set; }
    public DbSet<ServerData> servers { get; set; }
    public DbSet<UserData> users { get; set; }
    public DbSet<Preset> presets { get; set; }
    public DbSet<CustomDbTitle> custom_titles { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder o)
    {
        o.UseSqlite("Data Source=G:/Database/Boobot/data.db;Cache=Shared");
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder) {
        modelBuilder.Entity<Episode>().HasIndex(e => e.Guid, "episodes_Guid_uindex").IsUnique();
        // modelBuilder.Entity<UserData>().HasMany(e => e.Presets).WithOne(e => e.Owner).HasForeignKey(e => e.OwnerID);
        // modelBuilder.Entity<UserData>().HasOne(e => e.CurrentPreset).WithOne().HasForeignKey<UserData>(e => e.CurrentPresetID);
        modelBuilder.Entity<UserData>().HasMany(e => e.Presets).WithOne(e => e.Owner);
    }
}
public class Episode {
    public int Id { get; set; }
    public float Number { get; set; }
    public string Group { get; set; }
    [Required]
    public string Link { get; set; }
    public string? Guid { get; set; }
    [Required]
    public DateTime PubDate { get; set; }
    public int TitleId { get; set; }
    [Required]
    public Title Title { get; set; }
}

public class Title {
    public int Id { get; set; }
    [Required]
    public string Name { get; set; }
    public float LastEpisode { get; set; }
    public List<Episode> Episodes { get; set; }
    public List<ServerData> FollowingServers { get; set; } = new();
    public Title(string name) {
        Name = name;
        LastEpisode = 0;
        Episodes = new List<Episode>();
    }
}

public class Preset {
    public int Id { get; set; }
    public string Prompt { get; set; } = "";
    public string NegativePrompt { get; set; } = "";
    public string? ImageUrl { get; set; }
    public string Sampler { get; set; } = "Euler a";
    public string Model { get; set; } = "model.ckpt [925997e9]";
    public float DenoisingStrength { get; set; } = 0.75f;
    public int Seed { get; set; } = -1;
    public int Steps { get; set; } = 20;
    public float CfgScale { get; set; } = 9;
    public int Width { get; set; } = 512;
    public int Height { get; set; } = 512;
    // public ulong OwnerID { get; set; }
    // public UserData Owner { get; set; }
    public UserData Owner { get; set; }
    public bool Public { get; set; } = true;
}
    
public class UserData {
    public ulong Id { get; init; }
    public int eybrows { get; set; }
    public int karma { get; set; } = 0;
    // public Preset? CurrentPreset { get; set; }
    // public List<Preset> Presets { get; set; } = new();
    public int? CurrentPresetId { get; set; }
    public List<Preset> Presets { get; set; } = new();

    public UserData(ulong id) {
        Id = id;
    }
}

public class ServerData {
    public ulong Id { get; init; }
    public ulong? AnnouncementChannelId { get; set; }
    public ulong? VkChannelId { get; set; }
    public List<Title> Titles { get; set; } = new();

    public ServerData(ulong id) {
        Id = id;
    }
}

public class CustomDbTitle {
    public int Id { get; set; }
    [Required]
    public string Name { get; set; }
    [Required]
    public DateTime DayOfWeek { get; set; }
}