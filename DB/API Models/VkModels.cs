﻿namespace BoobotR.DB.API_Models; 

public class VkModels {
    public class ClientInfo
    {
        public List<string> button_actions { get; set; }
        public bool keyboard { get; set; }
        public bool inline_keyboard { get; set; }
        public bool carousel { get; set; }
        public int lang_id { get; set; }
    }
    public class Image
    {
        public string url { get; set; }
        public int width { get; set; }
        public int height { get; set; }
        public int with_padding { get; set; }
    }
    public class Sticker
    {
        public int sticker_id { get; set; }
        public int product_id { get; set; }
        public List<Image> images { get; set; }
        public bool is_allowed { get; set; }
    }
    public class Attachment
    {
        public string type { get; set; }
        public Sticker sticker { get; set; }
        public Photo photo { get; set; }
        public Video video { get; set; }
        public Wall wall { get; set; }
        public Doc doc { get; set; }
        public string style { get; set; }
    }

    public class Photo
    {
        public int album_id { get; set; }
        public int date { get; set; }
        public int id { get; set; }
        public int owner_id { get; set; }
        public string access_key { get; set; }
        public List<Size> sizes { get; set; }
        public string text { get; set; }
        public bool has_tags { get; set; }
    }

    public class Size
    {
        public int height { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public string url { get; set; }
    }
    
    public class Message
    {
        public int date { get; set; }
        public int from_id { get; set; }
        public int id { get; set; }
        public int @out { get; set; }
        public List<Attachment> attachments { get; set; }
        public int conversation_message_id { get; set; }
        public List<FwdMessage> fwd_messages { get; set; }
        public bool important { get; set; }
        public bool is_hidden { get; set; }
        public int peer_id { get; set; }
        public int random_id { get; set; }
        public string text { get; set; }
    }

    public class FwdMessage
    {
        public int date { get; set; }
        public int from_id { get; set; }
        public string text { get; set; }
        public List<Attachment> attachments { get; set; }
        public int conversation_message_id { get; set; }
        public int id { get; set; }
        public int peer_id { get; set; }
    }   

    public class Doc
    {
        public int id { get; set; }
        public int owner_id { get; set; }
        public string title { get; set; }
        public int size { get; set; }
        public string ext { get; set; }
        public int date { get; set; }
        public int type { get; set; }
        public string url { get; set; }
        //public Preview preview { get; set; }
        public string access_key { get; set; }
    }

    public class VkObject
    {
        public Message message { get; set; }
        public ClientInfo client_info { get; set; }
    }

    public class VkMessage
    {
        public int group_id { get; set; }
        public string type { get; set; }
        public string event_id { get; set; }
        public string v { get; set; }
        public VkObject @object { get; set; }
    }

    public class Wall
    {
        public int id { get; set; }
        public int from_id { get; set; }
        public int to_id { get; set; }
        public int date { get; set; }
        public int marked_as_ads { get; set; }
        public double short_text_rate { get; set; }
        public int compact_attachments_before_cut { get; set; }
        public string access_key { get; set; }
        public List<Attachment> attachments { get; set; }
        public bool is_favorite { get; set; }
        public int owner_id { get; set; }
        public string post_type { get; set; }
        public int signer_id { get; set; }
        public string text { get; set; }
    }

    public class Video
    {
        public string access_key { get; set; }
        public int can_add { get; set; }
        public int comments { get; set; }
        public int date { get; set; }
        public string description { get; set; }
        public int duration { get; set; }
        public List<Image> image { get; set; }
        // public List<FirstFrame> first_frame { get; set; }
        public int width { get; set; }
        public int height { get; set; }
        public int id { get; set; }
        public int? owner_id { get; set; }
        public string title { get; set; }
        public bool is_favorite { get; set; }
        public string track_code { get; set; }
        public int repeat { get; set; }
        public string type { get; set; }
        public int views { get; set; }
    }
}